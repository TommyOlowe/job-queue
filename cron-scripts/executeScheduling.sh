#!/usr/bin/env bash
# This script is copied into the cron sidecar container and is called through cron according to the scheduling plans

curl --header "Content-Type: application/json" \
--request POST \
--data "{\"jobGroup\": ${JOB_GROUP}}" \
http://${DOCKER_INTERNAL_API_HOST}:${DOCKER_INTERNAL_API_PORT}/api/queue/jobGroup
