#!/usr/bin/env bash

if [ -z ${CI_COMMIT_SHA+x} ]; then
  CI_COMMIT_SHA=latest
fi

echo "Building REF=${CI_COMMIT_SHA}"

#Build dev-job Image
echo -e "\n\n Build dev-job Image"
docker build -t as/dev-job:${CI_COMMIT_SHA} -f docker/image/dev-job/Dockerfile .

#Queue
echo -e "\n\n Build Queue Image"
docker build -t as/job-queue:${CI_COMMIT_SHA} -f docker/image/queue/Dockerfile .

#Cron-Sidecar
echo -e "\n\n Build Cron-Sidecar Image"
docker build -t as/job-queue-cron-sidecar:${CI_COMMIT_SHA} -f docker/image/cron-sidecar/Dockerfile .

