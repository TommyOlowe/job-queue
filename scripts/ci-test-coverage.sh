#!/usr/bin/env bash
set -euf pipefail
cd "$(dirname "$0")"
cd ..

if [ -z ${CI_COMMIT_SHA+x} ]; then
  CI_COMMIT_SHA=latest
fi

# Create docker network if it doesn't exist
NETWORK_NAME="queue-test-coverage-$CI_COMMIT_SHA"

if [[ "$(docker network ls --format "{{.Name}}")" =~ (^|[[:space:]])$NETWORK_NAME($|[[:space:]]) ]] ; then
	echo "docker network '$NETWORK_NAME' already exists, no need to create it."
else
	echo "create docker network '$NETWORK_NAME'"
	docker network create $NETWORK_NAME
fi

REDIS_CONTAINER=$(docker run -d -ti --network $NETWORK_NAME --hostname redis redis:3.2.0)
docker run --network $NETWORK_NAME --env-file .env.dev as/job-queue:${CI_COMMIT_SHA} /usr/app/scripts/test-coverage.sh 40

docker stop $REDIS_CONTAINER


