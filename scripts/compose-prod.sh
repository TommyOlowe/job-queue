#!/usr/bin/env bash

cmd=${@:-up}

set -euf pipefail
cd "$(dirname "$0")"
cd ..

pwd

# Create docker network if it doesn't exist
NETWORK_NAME="as-local"

if [[ "$(docker network ls --format "{{.Name}}")" =~ (^|[[:space:]])$NETWORK_NAME($|[[:space:]]) ]] ; then
	echo "docker network '$NETWORK_NAME' already exists, no need to create it."
else
	echo "create docker network '$NETWORK_NAME'"
	docker network create $NETWORK_NAME
fi

# docker-compose
NETWORK_NAME=${NETWORK_NAME} \
docker-compose \
-f docker/compose/prod.compose.yaml \
${cmd}

