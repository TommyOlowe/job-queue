#!/usr/bin/env bash

if [ -z ${1+x} ];
    then echo "Coverage per line is unset. Usage: ./test-coverage.sh <COVERAGE_PER_LINE>"; exit 1;
    else echo "Coverage per line is set to $1%";
fi

nyc --check-coverage --lines $1 --per-file --include=src/server/** mocha --recursive --exit test
