const d = new Date();
let currentCode = d.getTime();

//TODO If data for jobs is being loaded from a database in the future this very basic code has to be adapted accordingly.
function nextCode(){
    currentCode += 1;
    return currentCode.toString();
}

module.exports = {
    nextCode,
};
