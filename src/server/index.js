const app = require('./server');

const manager = require('./queue/manager');

const config = require('./config');

const port = config.queueApiPort;


manager.restoreState()
    .catch(err => console.log(err));

app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
});
