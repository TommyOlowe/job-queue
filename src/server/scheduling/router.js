const express = require('express');
const controller = require('./controller');

const router = express.Router();

router.get('/:augurCode/:jobGroupType', controller.getJobSchedule);

router.put('/', controller.addJobSchedule);

router.delete('/:augurCode/:jobGroupType', controller.deleteJobSchedule);

module.exports = router;
