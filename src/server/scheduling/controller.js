const scheduling = require('./scheduling');
const handleResponse = require('./utils').handleResponse;

/**
 * @param request
 * @param response
 * @returns {Promise<void>}
 */
async function getJobSchedule(request, response) {
    const augurCode = request.params.augurCode;
    const jobGroupType = request.params.jobGroupType;

    const data = await scheduling.getJobSchedule(augurCode, jobGroupType);

    if(!data.schedulingPlan){
        response.status(501).json({'status': 'error'});
        return;
    }

    //TODO Error handling
    response.status(201).json(
        {
            'status':'success',
            'data': data,
        });
}

/**
 * @param request
 * @param response
 * @returns {Promise<void>}
 */
async function addJobSchedule(request, response) {

    console.log(request.body);

    const schedulingPlan = request.body.schedulingPlan;
    const jobGroup = request.body.jobGroup;

    const status = await scheduling.addJobSchedule(schedulingPlan, jobGroup);
    //TODO Error handling
    response.status(201).json({'status':'success',});
}

/**
 * @param request
 * @param response
 * @returns {Promise<void>}
 */
async function deleteJobSchedule(request, response) {
    const augurCode = request.params.augurCode;
    const jobGroupType = request.params.jobGroupType;

    this.body = await scheduling.deleteJobSchedule(augurCode, jobGroupType);
    //TODO Error handling
    response.status(201).json({'status':'success',});
}

module.exports = {
    getJobSchedule,
    addJobSchedule,
    deleteJobSchedule,
};
