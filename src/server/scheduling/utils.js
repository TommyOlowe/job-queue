const config = require('../config');

const schedulePeriod = {
    HOURLY: 'hourly',
    DAILY: 'daily',
    WEEKLY: 'weekly',
    MONTHLY: 'monthly',
};

function handleResponse({ err, ...body }) {
    if (err) {
        console.log(err);
    }
    return body;
}

function convertError(err) {
    return {
        code: err.code,
        message: err.message,
        err,
    };
}

function convertToCronStr(schedulingPlan){
    const period = schedulingPlan.period;
    const hourOfDay = isEmpty(schedulingPlan.hourOfDay) ? '*' : schedulingPlan.hourOfDay;
    const minuteOfHour = isEmpty(schedulingPlan.minuteOfHour) ? '*' : schedulingPlan.minuteOfHour;
    const dayOfWeek = isEmpty(schedulingPlan.dayOfWeek) ? '*' : schedulingPlan.dayOfWeek;
    const dayOfMonth = isEmpty(schedulingPlan.dayOfMonth) ? '*' : schedulingPlan.dayOfMonth;

    switch (period) {
        case schedulePeriod.HOURLY:
            return `${minuteOfHour} * * * *`;
        case schedulePeriod.DAILY:
            return `${minuteOfHour} ${hourOfDay} * * *`;
        case schedulePeriod.WEEKLY:
            return `${minuteOfHour} ${hourOfDay} * * ${dayOfWeek}`;
        case schedulePeriod.MONTHLY:
            return `${minuteOfHour} ${hourOfDay} ${dayOfMonth} * ${dayOfWeek}`;
        default:
            return `${minuteOfHour} ${hourOfDay} ${dayOfMonth} * ${dayOfWeek}`;
    }
}

function convertToSchedulingPlan(cronStr){

    const parts = cronStr.split(" ").slice(0,4);

    const minuteOfHour = parts[0] === "*" ? null : parts[0];
    const hourOfDay = parts[1] === "*" ? null : parts[1];
    const dayOfMonth = parts[2] === "*" ? null : parts[2];
    const dayOfWeek = parts[4] === "*" ? null : parts[4];

    let period = null;

    if(minuteOfHour && !hourOfDay && !dayOfMonth && !dayOfWeek){
        period = schedulePeriod.HOURLY;
    }else if(minuteOfHour && hourOfDay && !dayOfMonth && !dayOfWeek){
        period = schedulePeriod.DAILY;
    }else if(minuteOfHour && hourOfDay && !dayOfMonth && dayOfWeek){
        period = schedulePeriod.WEEKLY;
    }else if(minuteOfHour && hourOfDay && dayOfMonth && dayOfWeek){
        period = schedulePeriod.MONTHLY;
    }

    const schedulingPlan = {'period': period};

    if(hourOfDay) schedulingPlan.hourOfDay = hourOfDay;
    if(minuteOfHour) schedulingPlan.minuteOfHour = minuteOfHour;
    if(dayOfWeek) schedulingPlan.dayOfWeek = dayOfWeek;
    if(dayOfMonth) schedulingPlan.dayOfMonth = dayOfMonth;

    return schedulingPlan;
}

function isEmpty(value){
    return !(value !== undefined && value !== null && value.toString().trim());
}

/**
 * Returns the file path + name of the cron scheduling plan file
 * @param augurCode
 * @param jobType
 * @returns {string}
 */
function getCronFileName(augurCode, jobType){
    return `${config.schedulingDirectory}/${augurCode}_${jobType}`;
}

module.exports = {
    handleResponse,
    convertError,
    convertToCronStr,
    convertToSchedulingPlan,
    getCronFileName,
};
