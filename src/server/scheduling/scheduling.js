/*
TODOS
- Promise from bluebird?
 */

const fs = require('fs');
const path = require('path');
const Promise = require('bluebird').Promise;
const config = require('../config');
const utils = require('./utils');
const activityLogger = require('../activityLogger');

Promise.promisifyAll(fs);

/**
 * Called by the router: Get a Job Schedule
 * @param augurCode
 * @param jobGroupType
 * @returns {Promise<*>}
 */
async function getJobSchedule(augurCode, jobGroupType) {
    try {
        const filePath = path.resolve(utils.getCronFileName(augurCode, jobGroupType));
        const cronStr = await fs.readFileAsync(filePath, 'utf8');
        const schedulingPlan = utils.convertToSchedulingPlan(cronStr);

        return {
            'augurCode': augurCode,
            'jobGroupType': jobGroupType,
            'schedulingPlan': schedulingPlan,
        };
    } catch (err) {
        console.log(err);
        return utils.convertError(err);
    }
}

/**
 * Called by the router: Add a Job Schedule
 * @param schedulingPlan
 * @param jobGroup
 * @returns {Promise<*>}
 */
async function addJobSchedule(schedulingPlan, jobGroup) {

    activityLogger.logActivity("scheduling: Add Job Schedule", {
        'schedulingPlan': schedulingPlan,
        'jobGroup': jobGroup,
    });

    const augurCode = jobGroup.augurCode;
    const jobGroupType = jobGroup.jobGroupType;

    const strJobGroup = JSON.stringify(jobGroup);

    try {
        const filePath = path.resolve(utils.getCronFileName(augurCode, jobGroupType));
        await fs.writeFileAsync(filePath,
            `${utils.convertToCronStr(schedulingPlan)} ` +
            `${config.cronUser} ` +
            `export JOB_GROUP='${strJobGroup}'; ` +
            `${config.cmdExecuteScheduling} \n`,
            'utf8'
        );

        return { success: true };
    } catch (err) {
        console.log(err);
        return utils.convertError(err);
    }
}

/**
 * Called by the router: Remove a Job Schedule
 * @param augurCode
 * @param jobGroupType
 * @returns {Promise<*>}
 */
async function deleteJobSchedule(augurCode, jobGroupType) {
    try {
        const filePath = path.resolve(utils.getCronFileName(augurCode, jobGroupType));
        await fs.unlinkAsync(filePath);

        return { success: true };
    } catch (err) {
        console.log(err);
        return utils.convertError(err);
    }
}

module.exports = {
    getJobSchedule,
    addJobSchedule,
    deleteJobSchedule,
};
