const config = require('../config');
const Redis = require('ioredis');

let redisClient = new Redis({
    port: config.redisPort,
    host: config.redisHost,
    db: config.redisDb, //This switch might be useful for testing later
});

module.exports = {
    redisClient,
};
