const express = require('express');
const controller = require('./controller');

const router = express.Router();

// Route to GET the summary
router.get('/', controller.getSummary);

// Route to POST a job group
router.post('/jobGroup', controller.addJobGroup);

// Route to PUT a new job state
router.put('/job/state', controller.updateJobState);

// Route to mark a job as failed via GET
router.get('/markfailed/:jobCode', controller.markJobAsFailed);

module.exports = router;
