const manager = require('./manager');
const activityLogger = require('../activityLogger');

const augurQueueCoordinator = require('./augur-queue/augurQueueCoordinator');
const executionQueueCoordinator = require('./execution-queue/executionQueueCoordinator');

function addJobGroup(request, response){
    //TODO proper way to check for valid request-body??

    const jobGroup = request.body.jobGroup;

    if(!jobGroup){
        response.status(501).json({'status':'jobGroup is undefined'});
        return;
    }
    /*
    if(!jobGroup.habitatCode){
        response.status(501).json({'status':'habitatCode is undefined'});
        return;
    }
    */
    if(!jobGroup.augurCode){
        response.status(501).json({'status':'augurCode is undefined'});
        return;
    }
    if(!jobGroup.jobGroupType){
        response.status(501).json({'status':'jobGroupType is undefined'});
        return;
    }
    if(!jobGroup.jobs){
        response.status(501).json({'status':'jobs is undefined'});
        return;
    }
    if(jobGroup.jobs.length < 1){
        response.status(501).json({'status':'jobs is empty'});
        return;
    }
    //TODO validate jobs (are all required fields (a) existing and (b) filled with valid values

    manager.addJobGroup(jobGroup)
    .then(() => response.status(201).json({'status':'success',
            'jobGroupCode':jobGroup.jobGroupCode,
            'jobCodes':jobGroup.jobs.map(job => job.jobCode)
        })).catch((err) => response.status(400).json(err));
}

function getSummary(request, response){
    Promise.all([augurQueueCoordinator.getSummary(), executionQueueCoordinator.getSummary()])
        .then(([augurQueues, executionQueues]) => {
            const summary = {
                'augurQueues': augurQueues,
                'executionQueues': executionQueues,
                'activities': activityLogger.activities,
            };
            response.status(201).json(summary)
        }).catch(err => response.status(400).json(err));
}

function updateJobState(request, response){
    const jobCode = request.body.jobCode ? request.body.jobCode.toString() : null;
    const newJobState = request.body.newJobState;

    console.log("jobCode: " + jobCode + " newJobState: " + newJobState);

    const responseState = manager.updateJobState(jobCode, newJobState);

    if(responseState === manager.updateStateResponse.OK){
        response.status(201).json({'status':'success'});
    }else if(responseState === manager.updateStateResponse.NOT_FOUND){
        console.log(`Job with jobCode: ${jobCode} not found - no state was updated.`);
        response.status(201).json({'status':'success'});
    } else{
        response.status(501).json({'status':'error'});
    }
}

function markJobAsFailed(request, response){
    const jobCode = request.params.jobCode;
    manager.updateJobState(jobCode, 'failed');
    response.status(201).json({'state':'success'});
}

module.exports = {
    addJobGroup,
    getSummary,
    updateJobState,
    markJobAsFailed
};
