const executionTypes = {
    'binary-classification': 'spark',
    'jupyter-notebook': 'jupyter',
    'dev-wait':'dev-wait',
};

module.exports = executionTypes;
