const WAITING = 'waiting';
const EXECUTING = 'executing';
const EXECUTED = 'executed';
const FAILED = 'failed';

module.exports = {
    WAITING,
    EXECUTING,
    EXECUTED,
    FAILED,
};
