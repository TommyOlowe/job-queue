const IGNORE = 'ignore';
const RETRY = 'retry';
const FAIL_JOB_GROUP = 'failJobGroup';

module.exports = {
    IGNORE,
    RETRY,
    FAIL_JOB_GROUP,
};
