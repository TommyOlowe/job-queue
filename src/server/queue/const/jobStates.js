const WAITING = 'waiting';
const WAITING_IN_EXECUTION_QUEUE = 'waitingInExecutionQueue';
const EXECUTING = 'executing';
const EXECUTED = 'executed';
const FAILED = 'failed';

module.exports = {
    WAITING,
    WAITING_IN_EXECUTION_QUEUE,
    EXECUTING,
    EXECUTED,
    FAILED,
};
