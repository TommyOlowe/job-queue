const jobGroupTypePriorities = {
    'initial-learning': 1,
    'learning': 2,
    'evaluation': 3,
    'prediction': 4,
};

module.exports = jobGroupTypePriorities;
