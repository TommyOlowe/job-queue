const config = require('../../config');
const Bull = require('bull');
const child_process = require('child_process');
const activityLogger = require('../../activityLogger');
const { emitter } = require('../jobEventBus');
const jobStates = require('../const/jobStates');

class ExecutionQueue {
    constructor(executionType) {
        this.executionType = executionType;
        this.jobQueue = new Bull(executionType, { prefix: 'alta-sigma:bull', redis: { host: config.redisHost, port: config.redisPort }, defaultJobOptions: { attempts: 1 } });

        this.addJob = this.addJob.bind(this);
        this.executeJob = this.executeJob.bind(this);

        this.jobQueue.process(this.executeJob);
    };

    /**
     * Adds a Job into the ExecutionQueue
     * @param job
     */
    addJob(job) {
        // 1. Push Job to the end of the execution queue
        return this.jobQueue.add(job).catch(err => console.log(err));
    }

    /**
     * 
     */
    getSummary() {
        return this.jobQueue.getJobs(['active', 'waiting', 'failed']).then(jobs => jobs.map(job => job.data)).catch(err => console.log(err));
    }

    getJobByID(jobID) {
        return this.jobQueue.getJob(jobID);
    }

    /**
     * Executes a job
     * @param {Bull.Job} bullJob
     */
    async executeJob(bullJob) {
        const job = bullJob.data;
        let cmd;
        switch (job.jobClass) {
            /* DEV-JOB */
            case 'dev-wait': {
                const seconds = job.jobArgs.seconds;
                const jobCode = job.jobCode;
                switch (config.jobExecutionMethod) {
                    case 'docker': {
                        cmd = `docker run -d --network ${config.dockerDashboardNetwork} --env SECONDS=${seconds} --env JOB_CODE=${jobCode} --env QUEUE_API_HOST=${config.dockerInternalApiHost} --env QUEUE_API_PORT=${config.dockerInternalApiPort} as/dev-job`;
                        break;
                    }
                    case 'kube': {
                        //TODO 1. Add kubectl-config file in Dockerfile 2. Implement post against Kubernetes
                        break;
                    }
                    default: {
                        console.log(`JOB_EXECUTION_METHOD: ${config.jobExecutionMethod} not known.`);
                        throw new Error(`JOB_EXECUTION_METHOD: ${config.jobExecutionMethod} not known.`);
                    }
                }
                break;
                // job.manager.updateJobState(job.jobCode, 'triggered');
            }
            /* BINARY-CLASSIFICATION IN SPARK */
            case 'binary-classification': {
                switch (config.jobExecutionMethod) {
                    case 'docker': {
                        cmd = `
CONTAINER_NAME="${job.augurCode}-${job.jobType}-$(date +%s)"

CONTAINER_ID=$(/usr/local/bin/docker create \\
-e JOB_ARG_JOB_TYPE="${job.jobType}" \\
-e JOB_ARG_AUGUR_CODE="${job.augurCode}" \\
-e JOB_ARG_JOB_CODE="${job.jobCode}" \\
-e JOB_SUPERVISOR_HOST='${config.jobSupervisorHost}' \\
-e JOB_SUPERVISOR_PORT='${config.jobSupervisorPort}' \\
-e CASSANDRA_HOST='${config.cassandraHost}' \\
-e CASSANDRA_USERNAME='${config.cassandraUsername}' \\
-e CASSANDRA_PASSWORD='${config.cassandraPassword}' \\\\
-e SPARK_CORES_MAX= '${config.sparkCoresMax}' \\
-e SPARK_EXECUTOR_MEMORY= '${config.sparkExecutorMemory}' \\
-e SPARK_MASTER='${config.sparkMaster}' \\
--network ${config.dockerDashboardNetwork} \\
--name $CONTAINER_NAME \\
${config.imageBinaryClassification})

/usr/local/bin/docker network connect ${config.dockerBackendNetwork} $CONTAINER_NAME

/usr/local/bin/docker start $CONTAINER_ID`;
                        break;
                    }
                    case 'kubernetes':{
                        cmd = `
CONTAINER_NAME="augur-${job.augurCode.toLowerCase()}-${job.jobType.toLowerCase()}-$(date +%s)-job"

NODE=$(kubectl get nodes -o jsonpath='{ $.items[*].status.addresses[?(@.type=="InternalIP")].address}')
                
kubectl run $CONTAINER_NAME \\
--image=${config.imageBinaryClassification} \\
--env="JOB_ARG_JOB_TYPE=${job.jobType}" \\
--env="JOB_ARG_AUGUR_CODE=${job.augurCode}" \\
--env="JOB_ARG_JOB_CODE=${job.jobCode}" \\
--env="JOB_SUPERVISOR_HOST=${config.jobSupervisorHost}" \\
--env="JOB_SUPERVISOR_PORT=${config.jobSupervisorPort}" \\
--env="MINIO_PORT=30090" \\
--env="MINIO_HOST=$NODE" \\
--env "MINIO_ACCESS_KEY=${config.minioAccessKey}" \\
--env="MINIO_SECRET_KEY=${config.minioSecretKey}" \\
--env="CASSANDRA_HOST=${config.cassandraHost}" \\
--env="CASSANDRA_USERNAME=${config.cassandraUsername}" \\
--env="CASSANDRA_PASSWORD=${config.cassandraPassword}" \\
--env="SPARK_MASTER=${config.sparkMaster}" \\
--env="SPARK_EXECUTOR_MEMORY=${config.sparkExecutorMemory}" \\
--env="SPARK_CORES_MAX=${config.sparkCoresMax}" \\
--env="SPARK_DRIVER_HOST=$NODE" \\
--env="DEPLOY_MODE=client" \\
--labels="spark=job" \\
--restart=Never
                        `;
                        break;
                    }
                    default: {
                        console.log(`JOB_EXECUTION_METHOD: ${config.jobExecutionMethod} not known.`);
                        throw new Error(`JOB_EXECUTION_METHOD: ${config.jobExecutionMethod} not known.`);
                    }
                }
                // job.manager.updateJobState(job.jobCode, 'triggered');
                break;
            }
            /* UNKNOWN JOBCLASS */
            default: {
                console.log("Unknown jobClass for execution: " + job.jobClass);
                throw new Error(`Unknown jobClass for execution: ${job.jobClass}`);
            }
        }
        console.log("------ CMD: " + cmd);
        return await this.executeBash(cmd, bullJob);
    }

    /**
     * Executes a bash script from a file
     * @param cmd {String}
     * @param {Bull.Job} bullJob
     */
    executeBash(cmd, bullJob) {
        const job = bullJob.data;
        const promise = new Promise((resolve, reject) => {
            console.log(`${this.executionType}:${job.jobCode}`);
            emitter.on(`${this.executionType}:${job.jobCode}`, state => {
                this.onStatusChange(state, job, bullJob, resolve, reject);
            });
            child_process.exec(cmd, (err, stdout, stderr) => {
                this.onProcessExecute(err, job, bullJob, reject);
            });
        });
        console.log("Execute child process.");
        return promise;
    }

    onProcessExecute(err, job, bullJob, reject) {
        if (err) {
            job.jobState = jobStates.FAILED;
            bullJob.update(job).catch(err => console.log('Error Updating Job Data'));
            reject(err);
        }
    }

    onStatusChange(state, job, bullJob, resolve, reject) {
        console.log(`[EXECUTION QUEUE] State is ${state}`);
        job.jobState = state;
        bullJob.update(job).catch(err => console.log('Error Updating Executed Job Data'));
        if (state === jobStates.EXECUTED || state === jobStates.FAILED) {
            emitter.removeAllListeners(`${this.executionType}:${job.jobCode}`);
            if (state === jobStates.EXECUTED) resolve();
            else if (state === jobStates.FAILED) reject('Job Failed !');
        }
    }
}

module.exports = ExecutionQueue;
