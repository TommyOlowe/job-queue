const executionTypes = require('../const/executionTypes');
const ExecutionQueue = require('./executionQueue');

const executionQueues = new Map();

/**
 * Adds a JobGroup into the corresponding ExecutionQueue for the specified jobClass (in job.jobClass)
 * @param job
 */
function addJob(job){

    console.log("AddJob " + job);

    const executionType = executionTypes[job.jobClass] || 'unknown-type!';

    if(!executionQueues.has(executionType)){
        executionQueues.set(executionType, new ExecutionQueue(executionType));
    }

    const executionQueue = executionQueues.get(executionType);

    job.jobState = 'waiting-in-execution-queue';
    return executionQueue.addJob(job);
}

/**
 * @returns {Array}
 */
async function getSummary(){
    const summary = [];
    if(executionQueues){
        for (let [executionType, executionQueue] of executionQueues) {
            summary.push({
                'executionType': executionType,
                'queue': await executionQueue.getSummary()
            });
        }
    }
    return summary;
}

async function getBullJob(job) {
    const executionType = executionTypes[job.jobClass];
    if (executionQueues.has(executionType)) {
        console.log(`Bull Job ID: ${job.bullJobID}`);
        const executionQueue = executionQueues.get(executionType);
        return await executionQueue.getJobByID(job.bullJobID);
    }
    return null;
}

function restoreExecutionQueues() {
    const allExecutionTypes = Object.values(executionTypes);
    for(let executionTypeIndex = 0; executionTypeIndex < allExecutionTypes.length; executionTypeIndex++){
        const executionType = allExecutionTypes[executionTypeIndex];
        if(!executionQueues.has(executionType)) {
            console.log(`Creating ${executionType} Execution Queue !`);
            executionQueues.set(executionType, new ExecutionQueue(executionType));
        }
    }
}

module.exports = {
    executionQueues,
    addJob,
    getSummary,
    getBullJob,
    restoreExecutionQueues
};
