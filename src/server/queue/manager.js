const codeGenerator = require('../helper/codeGenerator');

const executionTypes = require('./const/executionTypes');
const jobGroupStates = require('./const/jobGroupStates');
const jobStates = require('./const/jobStates');
const jobFailStrategies = require('./const/jobFailStrategies');
const jobGroupTypePriorities = require('./const/jobGroupTypePriorities');

const augurQueueCoordinator = require('./augur-queue/augurQueueCoordinator');
const executionQueueCoordinator = require('./execution-queue/executionQueueCoordinator');
const activityLogger = require('../activityLogger');
const { emitter } = require('./jobEventBus');
const { redisClient } = require('../redis/db');

const jobs = new Map();

/**
 * Central point for adding a JobGroup
 * @param jobGroup
 */
function addJobGroup(jobGroup){

    activityLogger.logActivity("manager: Add Job Group", jobGroup);

    jobGroup.jobGroupCode = jobGroup.jobGroupCode || codeGenerator.nextCode();
    jobGroup.jobGroupState = jobGroup.jobGroupState || jobGroupStates.WAITING;
    jobGroup.jobGroupTypePriority = jobGroup.jobGroupTypePriority || jobGroupTypePriorities[jobGroup.jobGroupType];

    const jobsArgs = [];
    jobGroup.jobs.forEach(job => {
        job.jobCode = job.jobCode || codeGenerator.nextCode();
        job.jobGroupCode = jobGroup.jobGroupCode;
        job.jobState = job.jobState || jobStates.WAITING;
        job.augurCode = jobGroup.augurCode;

        jobsArgs.push(job.jobCode.toString(), job.jobClass);
    });

    return redisClient.hmset('alta-sigma:manager:jobs', ...jobsArgs).then(() => {
        for(let index = 0;index < jobsArgs.length;index += 2) jobs.set(jobsArgs[index], jobsArgs[index + 1]);
        return augurQueueCoordinator.addJobGroup(jobGroup);
    });
}

/**
 * Central point for updating the state of a job
 * @param jobCode
 * @param newState
 */
function updateJobState(jobCode, newState){
    //TODO check for valid jobState transitions! (a failed job must never become executed a.s.o.)

    activityLogger.logActivity("manager: Update Job State", {
        'jobCode': jobCode,
        'newState': newState,
    });

    const jobClass = jobs.get(jobCode);
    if(!jobClass){
        console.log("manager.js: Job not found.");
        return updateStateResponse.NOT_FOUND;
    }

    console.log(`Emitting ${executionTypes[jobClass]}:${jobCode} ${newState}`);
    emitter.emit(`${executionTypes[jobClass]}:${jobCode}`, newState);

    return updateStateResponse.OK;
}

async function restoreState() {
    const jobFields = await redisClient.hkeys('alta-sigma:manager:jobs');
    if (jobFields.length > 0) {
        const jobValues = await redisClient.hmget('alta-sigma:manager:jobs', jobFields);
        jobFields.forEach((field, index) => jobs.set(field, jobValues[index]));
    }

    await executionQueueCoordinator.restoreExecutionQueues();

    await augurQueueCoordinator.restoreAugurQueues();
}

const updateStateResponse = {
    OK: "ok",
    NOT_FOUND: "not found",
    ERROR: "error"
};

module.exports = {
    addJobGroup,
    updateJobState,
    updateStateResponse,
    restoreState, 
};
