const Bull = require('bull');
const config = require('../../config');
const executionQueueCoordinator = require('../execution-queue/executionQueueCoordinator');
const activityLogger = require('../../activityLogger');
const jobStates = require('../const/jobStates');
const jobGroupStates = require('../const/jobGroupStates');
const executionTypes = require('../const/executionTypes');
const { emitter } = require('../jobEventBus');

class AugurQueue {
    constructor(habitatCode, augurCode) {
        this.habitatCode = habitatCode;
        this.augurCode = augurCode;
        this.jobGroupQueue = new Bull(augurCode, { prefix: 'alta-sigma:bull', redis: { host: config.redisHost, port: config.redisPort }, defaultJobOptions: { attempts: 1 } });

        this.pushJob = this.pushJob.bind(this);
        this.addJobGroup = this.addJobGroup.bind(this);

        this.jobGroupQueue.process(this.pushJob);
    };

    /**
     * Adds a JobGroup into the AugurQueue - this is where the logic for JobGroup-removal and prioritizing takes place.
     * @param jobGroup
     */
    async addJobGroup(jobGroup) {
        try {
            const waitingJobGroups = await this.jobGroupQueue.getWaiting();
            const similarJobGroups = waitingJobGroups.filter(job => job.data.jobGroupType === jobGroup.jobGroupType);
            await Promise.all(similarJobGroups.map(job => job.remove()));
        } catch (err) {
            throw err;
        }
        return this.jobGroupQueue.add(jobGroup, { priority: jobGroup.jobGroupTypePriority });
    }

    /**
     * Process Job Groups added to the Queue in order by pushing
     * each Job to the Execution Queue if and only if the previous
     * Job completes successfully, otherwise the Job Group fails.
     * @param {Bull.Job} bullJob
     */
    async pushJob(bullJob) {
        const jobGroup = bullJob.data;

        jobGroup.jobGroupState = jobGroupStates.EXECUTING;
        await bullJob.update(jobGroup);

        for (let jobIndex = 0; jobIndex < jobGroup.jobs.length; jobIndex++) {
            let job;
            const currentJobState = jobGroup.jobs[jobIndex].jobState;
            console.log(`Current Job State: ${currentJobState}`);

            if (currentJobState === jobStates.EXECUTED) {
                continue;
            } else if (currentJobState === jobStates.FAILED) {
                await this.failJobGroup(bullJob, jobIndex);
                throw new Error('Job Group Failed!');
            } else if (currentJobState === jobStates.WAITING) {
                job = await executionQueueCoordinator.addJob(jobGroup.jobs[jobIndex]);
                jobGroup.jobs[jobIndex].bullJobID = job.id;
                jobGroup.jobs[jobIndex].jobState = jobStates.WAITING_IN_EXECUTION_QUEUE;
                await bullJob.update(jobGroup);
            } else if (currentJobState === jobStates.WAITING_IN_EXECUTION_QUEUE || currentJobState === jobStates.EXECUTING) {
                job = await executionQueueCoordinator.getBullJob(jobGroup.jobs[jobIndex]);
            }

            try {
                emitter.on(`${executionTypes[job.data.jobClass]}:${job.data.jobCode}`, state => {
                    if (state === jobStates.EXECUTING) {
                        jobGroup.jobs[jobIndex].jobState = state;
                        bullJob.update(jobGroup).catch((err) => console.log(err));
                    }
                });
                // This resolves with the Job's result.
                await job.finished();
                jobGroup.jobs[jobIndex].jobState = jobStates.EXECUTED;
                await bullJob.update(jobGroup);
            } catch (err) {
                console.log(err);
                await this.failJobGroup(bullJob, jobIndex);
                throw err;
            }
        }
        jobGroup.jobGroupState = jobGroupStates.EXECUTED;
        await bullJob.update(jobGroup);
    }

    /**
     * Get the summary of the augurQueue
     * @returns {Array}
     */
    getSummary() {
        return this.jobGroupQueue.getJobs(['active', 'waiting', 'failed']).then(jobs => jobs.map(job => job.data));
    }

    /**
     * 
     * @param {Bull.Job<any>} bullJob 
     * @param {*} jobIndex 
     */
    async failJobGroup(bullJob, jobIndex) {
        const jobGroup = bullJob.data;
        jobGroup.jobs.forEach((job, index) => {
            if (index >= jobIndex) jobGroup.jobs[index].jobState = jobStates.FAILED;
        });
        jobGroup.jobGroupState = jobGroupStates.FAILED;
        await bullJob.update(jobGroup);
        const jobGroups = await this.jobGroupQueue.getJobs(['waiting']);
        await Promise.all(jobGroups.map(job => job.moveToFailed()));
    }
}

module.exports = AugurQueue;
