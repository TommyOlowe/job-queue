const AugurQueue = require('./augurQueue');
const { redisClient } = require('../../redis/db');

const augurQueues = new Map();

/**
 * Adds a JobGroup into the corresponding AugurQueue for the specified augur (in jobGroup.augurCode)
 * @param jobGroup
 */
function addJobGroup(jobGroup) {
    console.log(jobGroup);

    const augurCode = jobGroup.augurCode;
    const habitatCode = jobGroup.habitatCode;

    return redisClient.hset('alta-sigma:aqc:aq', augurCode, habitatCode).then(() => {
        if (!augurQueues.has(augurCode)) {
            augurQueues.set(augurCode, new AugurQueue(habitatCode, augurCode));
        }

        const augurQueue = augurQueues.get(augurCode);
        return augurQueue.addJobGroup(jobGroup);
    });
}

/**
 * Get the summary about all augurQueues
 * @returns {Array}
 */
async function getSummary() {
    const summary = [];
    if (augurQueues) {
        for (let [augurCode, augurQueue] of augurQueues) {
            summary.push({
                'augurCode': augurCode,
                'queue': await augurQueue.getSummary()
            });
        }
    }
    return summary;
}

async function restoreAugurQueues() {
    const augurCodes = await redisClient.hkeys('alta-sigma:aqc:aq');

    if (augurCodes.length <= 0) return;

    const habitatCodes = await redisClient.hmget('alta-sigma:aqc:aq', augurCodes);

    augurCodes.forEach((augurCode, index) => {
        if (!augurQueues.has(augurCode)) {
            augurQueues.set(augurCode, new AugurQueue(habitatCodes[index], augurCode));
        }
    });
}

module.exports = {
    augurQueues,
    addJobGroup,
    getSummary,
    restoreAugurQueues,
};
