const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const swaggerUi = require('swagger-ui-express');
const yaml = require('js-yaml');
const fs = require('fs');

const queue = require('./queue');
const scheduling = require('./scheduling');

const app = express();

app.use(bodyParser.json());

app.get('/', (request, response) => response.redirect('/api-docs'));

const swaggerDocument = yaml.safeLoad(fs.readFileSync('swagger.yml', 'utf8'));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument)); //Route for the Swagger-UI

app.use(morgan('common', { immediate: true }));

app.use('/api/queue', queue.router); //Routes for the JobQueue
app.use('/api/scheduling', scheduling.router); //Routes for the Scheduling-Plans

module.exports = app;