const util = require('util');

const activities = [];

/**
 * Adds a message to the activity log
 * @param message
 * @param json
 */
function logActivity(message, json){
    const date = new Date();
    let hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;
    let min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;
    let sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;
    const year = date.getFullYear();
    let month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    let day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    const timestamp =  /* year + "-" + month + "-" + day + " " + */ hour + ":" + min + ":" + sec;

    activities.push({
        'timestamp':timestamp,
        'message': message,
        'json': util.inspect(json,{showHidden: false, depth: null}),
    });
}

module.exports={
    logActivity,
    activities,
};
