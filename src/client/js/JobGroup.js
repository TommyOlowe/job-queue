import React, { Component } from 'react';

export default class JobGroup extends Component {
    constructor(props) {
        super(props);
    }

    render(){

        const jobGroup = this.props.jobGroup;

        return <div className={"job-group"}>
            <div className={"job-group-state-container"}>
                <div className={"job-group-state jg-" + jobGroup.jobGroupState} />
                <div>
                    <p>jgType: {jobGroup.jobGroupType}</p>
                    <p>jgState: {jobGroup.jobGroupState}</p>
                    <p>jgCode: {jobGroup.jobGroupCode}</p>
                    <p>jgTypePriority: {jobGroup.jobGroupTypePriority}</p>
                </div>
            </div>
            <div className={"job-container"}>
                {
                    jobGroup.jobs && jobGroup.jobs.map(job =>
                        <div className={"job " + job.jobState} key={job.jobCode}>
                            <p>JobClass: {job.jobClass}</p>
                            <p>JobType: {job.jobType}</p>
                            <p>JobState: {job.jobState}</p>
                            <p>JobCode: {job.jobCode}</p>
                        </div>
                    )
                }
            </div>
        </div>

    }
}
