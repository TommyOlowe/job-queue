import React, { Component } from 'react';
import JobGroup from './JobGroup';

export default class ExecutionQueue extends Component {
    constructor(props){
        super(props);
    }

    render(){
        const queue = this.props.queue;
        const executionType = this.props.executionType;

        const filled = queue.map(job =>
            <div className={"ex-job " + job.jobState} key={"ex-" + job.jobCode}>
                <p>JobClass: {job.jobClass}</p>
                <p>JobType: {job.jobType}</p>
                <p>JobState: {job.jobState}</p>
                <p>JobCode: {job.jobCode}</p>
            </div>
        );

        const empty = <div className={"ex-job empty"}>
                        <p>Empty Queue</p>
                    </div>;

        return(
            <div className="execution-queue">
                <div className={"execution-detail"}>
                    <p>Execution Type: {executionType}</p>
                </div>
                <div className={"ex-job-container"}>
                    {queue && queue.length === 0 ? empty : filled}
                </div>
            </div>
        );
    }
}
