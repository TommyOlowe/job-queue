import React, { Component } from 'react';
import '../css/styles.css';
import AugurQueue from './AugurQueue.js';
import ExecutionQueue from './ExecutionQueue.js';
import JSONPretty from 'react-json-pretty';

export default class App extends Component {
    state = {};
    constructor(props){
        super(props);
    }

    componentDidMount() {
        console.log("Component did mount.");
        fetch('/api/queue')
            .then(res => res.json())
            .then(res => this.setState({
                augurQueues: res.augurQueues,
                executionQueues: res.executionQueues,
                activities: res.activities
            }));
    }

    render() {
        return (
            <div className={"body-container"}>
                <div className={"left-container"}>
                    <h1>Activity Log</h1>
                    <div className={"activity-container"}>
                        {
                            this.state.activities && this.state.activities.map(function (activity) {
                                return <div className={"activity-item"}>
                                            <div className={"activity-item-head"}>
                                                <p className={"activity-item-ts"}>{activity.timestamp}</p>
                                                <p className={"activity-item-message"}>{activity.message}</p>
                                            </div>
                                            <div className={"activity-item-body"}>
                                                <JSONPretty className={"activity-item-json"} id="json-pretty" json={activity.json} />
                                            </div>
                                        </div>
                            })
                        }
                    </div>
                </div>
                <div className={"main-container"}>
                    <h1>Augur Queues</h1>
                    <div className={"augur-queue-container"}>
                        {
                            this.state.augurQueues && this.state.augurQueues.map(function(e){
                                return <AugurQueue queue={e.queue} augurCode={e.augurCode} key={e.augurCode}/>
                            })
                        }
                    </div>
                    <h1>Execution Queues</h1>
                    <div className={"execution-queue-container"}>
                        {
                            this.state.executionQueues && this.state.executionQueues.map(function (e){
                                return <ExecutionQueue queue={e.queue} executionType={e.executionType} key={e.executionType}/>
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}
