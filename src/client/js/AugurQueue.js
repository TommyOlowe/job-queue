import React, { Component } from 'react';
import JobGroup from './JobGroup';

export default class AugurQueue extends Component {
    constructor(props){
        super(props);
    }

    render(){
        const augurCode = this.props.augurCode;
        const queue = this.props.queue;



        const filled = this.props.queue && this.props.queue.map(jobGroup =>
                            <JobGroup jobGroup={jobGroup} key={jobGroup.jobGroupCode} />
                        );

        const empty = <div className={"ex-job empty"}>
            <p>Empty Queue</p>
        </div>;

        return(
            <div className="augur-queue">
                <div className={"augur-detail"}>
                    <p>AugurCode: {augurCode}</p>
                </div>
                <div className={"job-group-container"}>
                    {queue && queue.length === 0 ? empty : filled}
                </div>
            </div>
        );
    }
}
