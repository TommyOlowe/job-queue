# Run 
## dev  
Run locally using `npm run dev`. 
- Swagger UI is available at [http://localhost:3011/api-docs](http://localhost:3011/api-docs)
- UI for the Queue-State is available at [http://localhost:3006/](http://localhost:3006/)

## Tests and Coverage
Run the tests using `npm run test-unit`.

Check the coverage using `npm run coverage`.

# Env-Variables
```bash
QUEUE_API_HOST=localhost #Host of the Queue-API
QUEUE_API_PORT=3011 #Port of the Queue-API
JOB_EXECUTION_METHOD=docker #Should the jobs executed with Docker (docker) or Kubernetes (kube)?
POP_FAILED_JOBS #Should failed jobs be simply removed from both the Queues?
DOCKER_INTERNAL_API_HOST=queue #Host on which the api host can be reached from inside a docker container  
DOCKER_INTERNAL_API_PORT=3011 #Port on which the api host can be reached from inside a docker container 
``` 
 
# API Calls
## Add Job  
`POST` to `localhost:3011/api/queue/jobGroup` 
```json
{
	"jobGroup":{	
		"augurCode":"ABC",
		"habitatCode":"def",
		"jobGroupType":"learning",
		"jobs": [
			{
				"jobClass": "binary-classification", 
				"jobType": "learning", 
				"jobArgs": {}
			},
			{
				"jobClass": "jupyter-notebook", 
				"jobArgs": {}
			},
            {
                "jobClass": "dev-wait", 
                "jobArgs": {"seconds":"10"}
            }
		]
	}
}
```

## Update job state 
`PUT` to `localhost:3011/api/queue/job/state` 
```json
{
	"augurCode": "GHIJK",
    "jobCode": "2",
    "newJobState": "executed"
}
```

## Get Queue states summary 
`GET` to `localhost:3011/api/queue/` 

