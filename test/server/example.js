const assert = require('assert');

/**
 * This is the example from the official mocha docs, testing the indexOf()-function of an Array
 */
describe('Array', function() {
    describe('#indexOf()', function() {
        it('should return -1 when the value is not present', function() {
            assert.equal([1,2,3].indexOf(4), -1);
        });
    });
});
