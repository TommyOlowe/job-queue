const fs = require('fs');
const path = require('path');

const chai = require('chai');
const sinon = require('sinon');

const config = require('../../../src/server/config');
const utils = require('../../../src/server/scheduling/utils');
const scheduling = require('../../../src/server/scheduling/scheduling');

describe('Scheduling', () => {
    /** @type {sinon.SinonSandbox} */
    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('getJobSchedule', () => {
        it('should return the job schedule', done => {
            const augurCode = 'ABCDE';
            const jobGroupType = 'learning';
            const schedulingPlan = {
                period: 'hourly',
                minuteOfHour: 52
            };
            const jobGroup = {
                augurCode: 'SCHED',
                habitatCode: 'def',
                jobGroupType: 'learning',
                jobs: [
                    {
                        jobClass: 'binary-classification',
                        jobType: 'learning',
                        jobArgs: {},
                    }
                ]
            };
            const jobGroupString = JSON.stringify(jobGroup);
            const schedulingFilePath = `${config.schedulingDirectory}/${augurCode}_${jobGroupType}`;
            const cronString = `${schedulingPlan.minuteOfHour} * * * *`;
            const fileContent = `${cronString} ` +
                `${config.cronUser} ` +
                `export JOB_GROUP='${jobGroupString}'; ` +
                `${config.cmdExecuteScheduling} \n`;

            const getCronFileNameMock = sinon.mock(utils);
            getCronFileNameMock.expects('getCronFileName')
                .once()
                .withExactArgs(augurCode, jobGroupType)
                .returns(schedulingFilePath);

            const pathResolveMock = sandbox.mock(path);
            pathResolveMock.expects('resolve')
                .once()
                .withExactArgs(schedulingFilePath)
                .returns(schedulingFilePath);

            const readFileMock = sandbox.mock(fs);
            readFileMock.expects('readFileAsync')
                .once()
                .withExactArgs(schedulingFilePath, 'utf8')
                .returns(fileContent);

            const convertToSchedulingPlanMock = sandbox.mock(utils);
            convertToSchedulingPlanMock.expects('convertToSchedulingPlan')
                .once()
                .withExactArgs(fileContent)
                .returns(schedulingPlan);

            scheduling.getJobSchedule(augurCode, jobGroupType)
                .then(schedule => {
                    getCronFileNameMock.verify();
                    pathResolveMock.verify();
                    readFileMock.verify();
                    convertToSchedulingPlanMock.verify();

                    chai.expect(Object.keys(schedule)).to.have.lengthOf(3);
                    chai.expect(schedule.augurCode).to.be.equal(augurCode);
                    chai.expect(schedule.jobGroupType).to.be.equal(jobGroupType);
                    chai.expect(schedule.schedulingPlan).to.be.equal(schedulingPlan);
                    done();
                })
                .catch(err => done(err));
        });
    });

    describe('addJobSchedule', () => {
        it('should add the scheduling job successfully', done => {
            const schedulingPlan = {
                period: 'hourly',
                minuteOfHour: 52
            };
            const jobGroup = {
                augurCode: 'SCHED',
                habitatCode: 'def',
                jobGroupType: 'learning',
                jobs: [
                    {
                        jobClass: 'binary-classification',
                        jobType: 'learning',
                        jobArgs: {},
                    }
                ]
            };
            const schedulingFilePath = `${config.schedulingDirectory}/${jobGroup.augurCode}_${jobGroup.jobGroupType}`;
            const cronString = `${schedulingPlan.minuteOfHour} * * * *`;
            const jobGroupString = JSON.stringify(jobGroup);

            const getCronFileNameMock = sinon.mock(utils);
            getCronFileNameMock.expects('getCronFileName')
                .once()
                .withExactArgs(jobGroup.augurCode, jobGroup.jobGroupType)
                .returns(schedulingFilePath);

            const writeFileMock = sandbox.mock(fs);
            writeFileMock.expects('writeFileAsync')
                .once()
                .withExactArgs(schedulingFilePath,
                    `${cronString} ` +
                    `${config.cronUser} ` +
                    `export JOB_GROUP='${jobGroupString}'; ` +
                    `${config.cmdExecuteScheduling} \n`,
                    'utf8')
                .returns(Promise.resolve());

            const resolveMock = sandbox.mock(path);
            resolveMock.expects('resolve')
                .once()
                .withExactArgs(schedulingFilePath)
                .returns(schedulingFilePath);

            const convertToCronStrMock = sandbox.mock(utils);
            convertToCronStrMock.expects('convertToCronStr')
                .once()
                .withExactArgs(schedulingPlan)
                .returns(cronString);

            scheduling.addJobSchedule(schedulingPlan, jobGroup)
                .then(status => {
                    getCronFileNameMock.verify();
                    writeFileMock.verify();
                    resolveMock.verify();
                    convertToCronStrMock.verify();

                    chai.expect(status.success).to.be.true;

                    done();
                })
                .catch(err => done(err));
        });
    });

    describe('deleteJobSchedule', () => {
        it('should delete the scheduling job successfully', done => {
            const augurCode = 'ABCDE';
            const jobGroupType = 'learning';
            const schedulingFilePath = `${config.schedulingDirectory}/${augurCode}_${jobGroupType}`;

            const getCronFileNameMock = sinon.mock(utils);
            getCronFileNameMock.expects('getCronFileName')
                .once()
                .withExactArgs(augurCode, jobGroupType)
                .returns(schedulingFilePath);

            const resolveMock = sandbox.mock(path);
            resolveMock.expects('resolve')
                .once()
                .withExactArgs(schedulingFilePath)
                .returns(schedulingFilePath);

            const unlinkMock = sandbox.mock(fs);
            unlinkMock.expects('unlinkAsync')
                .once()
                .withExactArgs(schedulingFilePath)
                .returns(Promise.resolve());

            scheduling.deleteJobSchedule(augurCode, jobGroupType)
                .then(status => {
                    getCronFileNameMock.verify();
                    resolveMock.verify();
                    unlinkMock.verify();

                    chai.expect(status.success).to.be.true;

                    done();
                })
                .catch(err => done(err));
        });
    });
});