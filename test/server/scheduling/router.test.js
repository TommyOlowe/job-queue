const chai = require('chai');
const sinon = require('sinon');
const supertest = require('supertest');

const app = require('../../../src/server/server');
const scheduling = require('../../../src/server/scheduling/scheduling');

describe('Scheduling Router', () => {
    /** @type {sinon.SinonSandbox} */
    let sandbox;
    /** @type {supertest.SuperTest<supertest.Test>} */
    let request;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        request = supertest(app);
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('GET /api/scheduling/:augurCode/:jobGroupType', () => {
        it('should return the scheduling plan and status 201', done => {
            const augurCode = 'ABCDE';
            const jobGroupType = 'learning';
            const schedulingPlan = {
                period: 'hourly',
                minuteOfHour: 52
            };
            const jobSchedule = {
                augurCode,
                jobGroupType,
                schedulingPlan,
            };

            const getJobScheduleMock = sandbox.mock(scheduling);
            getJobScheduleMock.expects('getJobSchedule')
                .once()
                .withExactArgs(augurCode, jobGroupType)
                .returns(Promise.resolve(jobSchedule));

            request.get(`/api/scheduling/${augurCode}/${jobGroupType}`)
                .expect(201)
                .end((err, res) => {
                    if (err) return done(err);

                    getJobScheduleMock.verify();

                    chai.expect(Object.keys(res.body)).to.have.lengthOf(2);
                    chai.expect(res.body.status).to.be.equal('success');
                    chai.expect(res.body.data).to.deep.equal(jobSchedule);
                    done();
                });
        });

        it('should return status 501', done => {
            const augurCode = 'ABCDE';
            const jobGroupType = 'learning';
            const getScheduleError = {
                code: 1,
                message: 'An Error Occured',
            };
            const getJobScheduleMock = sandbox.mock(scheduling);
            getJobScheduleMock.expects('getJobSchedule')
                .once()
                .withExactArgs(augurCode, jobGroupType)
                .returns(Promise.resolve(getScheduleError));

            request.get(`/api/scheduling/${augurCode}/${jobGroupType}`)
                .expect(501)
                .end((err, res) => {
                    if (err) return done(err);

                    getJobScheduleMock.verify();

                    chai.expect(Object.keys(res.body)).to.have.lengthOf(1);
                    chai.expect(res.body.status).to.be.equal('error');
                    done();
                });
        });
    });

    describe('PUT /api/scheduling', () => {
        it('should add a job schedule with status 201', done => {
            const schedulingPlan = {
                period: 'hourly',
                minuteOfHour: 52
            };
            const jobGroup = {
                augurCode: 'SCHED',
                habitatCode: 'def',
                jobGroupType: 'learning',
                jobs: [
                    {
                        jobClass: 'binary-classification',
                        jobType: 'learning',
                        jobArgs: {},
                    }
                ]
            };

            const addJobScheduleMock = sandbox.mock(scheduling);
            addJobScheduleMock.expects('addJobSchedule')
                .once()
                .withExactArgs(schedulingPlan, jobGroup)
                .returns(Promise.resolve({ success: true }));

            request.put('/api/scheduling')
                .send({ schedulingPlan, jobGroup })
                .expect(201)
                .end((err, res) => {
                    if (err) return done(err);

                    addJobScheduleMock.verify();

                    chai.expect(Object.keys(res.body)).to.have.lengthOf(1);
                    chai.expect(res.body.status).to.be.equal('success');
                    done();
                });
        });
    });

    describe('DELETE /api/scheduling/:augurCode/:jobGroupType', () => {
        it('should delete a job schedule successfully with status 201', done => {
            const augurCode = 'ABCDE';
            const jobGroupType = 'learning';

            const deleteJobScheduleMock = sandbox.mock(scheduling);
            deleteJobScheduleMock.expects('deleteJobSchedule')
                .once()
                .withExactArgs(augurCode, jobGroupType)
                .returns(Promise.resolve({ success: true }));

            request.delete(`/api/scheduling/${augurCode}/${jobGroupType}`)
                .expect(201)
                .end((err, res) => {
                    if (err) return done(err);

                    deleteJobScheduleMock.verify();

                    chai.expect(Object.keys(res.body)).to.have.lengthOf(1);
                    chai.expect(res.body.status).to.be.equal('success');

                    done();
                });
        });
    });
});