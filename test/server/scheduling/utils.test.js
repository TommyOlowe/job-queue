const chai = require('chai');
const sinon = require('sinon');

const config = require('../../../src/server/config');
const utils = require('../../../src/server/scheduling/utils');

describe('Scheduling Utils', () => {
    /** @type {sinon.SinonSandbox} */
    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('handleResponse', () => {
        it('should return the body', () => {
            const response = {
                err: new Error('An Error Message'),
                code: 2,
                message: 'An Error Message',
            };
            const body = utils.handleResponse(response);

            chai.expect(Object.keys(body)).to.have.lengthOf(2);
            chai.expect(body).to.deep.equal({ code: response.code, message: response.message });
        });
    });

    describe('convertError', () => {
        it('should return an object with the error, error message and error code', () => {
            const error = new Error('An Error Message');

            const convertedError = utils.convertError(error);

            chai.expect(Object.keys(convertedError)).to.have.lengthOf(3);
            chai.expect(convertedError.err).to.deep.equal(error)
            chai.expect(convertedError.message).to.deep.equal(error.message)
            chai.expect(convertedError.code).to.deep.equal(error.code)
        });
    });

    describe('convertToCronStr', () => {
        it('should convert a scheduling plan to a cron string', () => {
            const schedulingPlan = {
                period: 'hourly',
                minuteOfHour: 52
            };
            chai.expect(utils.convertToCronStr(schedulingPlan)).to.be.equal(`${schedulingPlan.minuteOfHour} * * * *`);
        });
    });

    describe('getCronFileName', () => {
        it('should return the Cron filename given the Augur Code and JobGroup Type', () => {
            const augurCode = 'ABCDE';
            const jobGroupType = 'learning';

            const cronFilename = utils.getCronFileName(augurCode, jobGroupType);

            chai.expect(cronFilename).to.be.equal(`${config.schedulingDirectory}/${augurCode}_${jobGroupType}`);
        });
    });
});