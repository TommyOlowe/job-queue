const chai = require('chai');
const Bull = require('bull');
const sinon = require('sinon');
const EventEmitter = require('events');
const child_process = require('child_process');

const config = require('../../../../src/server/config');
const jobStates = require('../../../../src/server/queue/const/jobStates');
const executionTypes = require('../../../../src/server/queue/const/executionTypes');
const ExecutionQueue = require('../../../../src/server/queue/execution-queue/executionQueue');

const jobStateMatcher = state => sinon.match(job => job.jobState === state);

describe('Execution Queue', () => {
    /** @type {sinon.SinonSandbox} */
    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('addJob', () => {
        it('should add a Job to the Execution Queue successfully', done => {
            const job = {
                jobCode: 1,
                jobClass: 'binary-classification',
            };

            const addMock = sandbox.mock(Bull.prototype);
            addMock.expects('add')
                .once()
                .withExactArgs(job)
                .returns(Promise.resolve(job));

            const executionQueue = new ExecutionQueue('spark');
            executionQueue.addJob(job)
                .then(addedJob => {
                    addMock.verify();
                    chai.expect(addedJob).to.be.equal(job);
                    done();
                }).catch(err => done(err));
        });
    });

    describe('getSummary', () => {
        it('should return active, waiting and failed Jobs', done => {
            const jobs = [{
                id: 1,
                data: {
                    jobCode: 2,
                    jobClass: 'binary-classification',
                },
            }];
            const getJobsMock = sandbox.mock(Bull.prototype);
            getJobsMock.expects('getJobs')
                .once()
                .withExactArgs(['active', 'waiting', 'failed'])
                .returns(Promise.resolve(jobs));

            const executionQueue = new ExecutionQueue('spark');

            executionQueue.getSummary()
                .then(jobsSummary => {
                    getJobsMock.verify();
                    chai.expect(jobsSummary).to.deep.equal(jobs.map(job => job.data));
                    done();
                })
                .catch(err => done(err));
        });
    });

    describe('getJobByID', () => {
        it('should return the Bull Job given its ID', done => {
            const job = {
                id: 1,
                data: {
                    jobCode: 2,
                    jobClass: 'binary-classification',
                },
            };

            const getJobMock = sandbox.mock(Bull.prototype);
            getJobMock.expects('getJob')
                .once()
                .withExactArgs(job.id)
                .returns(Promise.resolve(job));

            const executionQueue = new ExecutionQueue('spark');

            executionQueue.getJobByID(job.id)
                .then(job => {
                    getJobMock.verify();
                    chai.expect(job).to.deep.equal(job);
                    done();
                })
                .catch(err => done(err));
        });
    });

    describe('executeJob', () => {
        it('should execute the `binary-classification` Job by building the correct docker command and calling `executeBash`', done => {
            const bullJob = sandbox.createStubInstance(Bull.Job);
            const job = {
                augurCode: 'GHIJK',
                jobType: 'learning',
                jobCode: 1,
                jobClass: 'binary-classification',
            };
            bullJob.data = job;

            sandbox.stub(config, 'jobExecutionMethod').value('docker');

            const cmd = `
CONTAINER_NAME="${job.augurCode}-${job.jobType}-$(date +%s)"

CONTAINER_ID=$(/usr/local/bin/docker create \\
-e JOB_ARG_JOB_TYPE="${job.jobType}" \\
-e JOB_ARG_AUGUR_CODE="${job.augurCode}" \\
-e JOB_ARG_JOB_CODE="${job.jobCode}" \\
-e JOB_SUPERVISOR_HOST='${config.jobSupervisorHost}' \\
-e JOB_SUPERVISOR_PORT='${config.jobSupervisorPort}' \\
-e CASSANDRA_HOST='${config.cassandraHost}' \\
-e SPARK_CORES_MAX= '${config.sparkCoresMax}' \\
-e SPARK_EXECUTOR_MEMORY= '${config.sparkExecutorMemory}' \\
-e SPARK_MASTER='${config.sparkMaster}' \\
--network ${config.dockerDashboardNetwork} \\
--name $CONTAINER_NAME \\
${config.imageBinaryClassification})

/usr/local/bin/docker network connect ${config.dockerBackendNetwork} $CONTAINER_NAME

/usr/local/bin/docker start $CONTAINER_ID`;

            const executionQueue = sandbox.createStubInstance(ExecutionQueue);
            executionQueue.executeBash = sandbox.mock(ExecutionQueue.prototype)
                .expects('executeBash')
                .once()
                .withExactArgs(cmd, bullJob)
                .returns(Promise.resolve());

            executionQueue.executeJob.restore();
            executionQueue.executeJob(bullJob)
                .then(() => {
                    executionQueue.executeBash.verify();
                    done();
                })
                .catch(err => done(err));
        });

        it('should execute the `dev-wait` Job by building the correct docker command and calling `executeBash`', done => {
            const bullJob = sandbox.createStubInstance(Bull.Job);
            const job = {
                augurCode: 'GHIJK',
                jobType: 'learning',
                jobCode: 1,
                jobClass: 'dev-wait',
                jobArgs: {
                    seconds: 5,
                },
            };
            bullJob.data = job;

            sandbox.stub(config, 'jobExecutionMethod').value('docker');
            sandbox.stub(config, 'dockerDashboardNetwork').value('as-local');
            sandbox.stub(config, 'dockerInternalApiHost').value('host');
            sandbox.stub(config, 'dockerInternalApiPort').value('8080');

            const cmd = `docker run -d --network ${config.dockerDashboardNetwork} --env SECONDS=${job.jobArgs.seconds} --env JOB_CODE=${job.jobCode} --env QUEUE_API_HOST=${config.dockerInternalApiHost} --env QUEUE_API_PORT=${config.dockerInternalApiPort} as/dev-job`;

            const executionQueue = sandbox.createStubInstance(ExecutionQueue);
            executionQueue.executeBash = sandbox.mock(ExecutionQueue.prototype)
                .expects('executeBash')
                .once()
                .withExactArgs(cmd, bullJob)
                .returns(Promise.resolve());

            executionQueue.executeJob.restore();
            executionQueue.executeJob(bullJob)
                .then(() => {
                    executionQueue.executeBash.verify();
                    done();
                })
                .catch(err => done(err));
        });

        it('should throw an error if `jobClass` is not defined', done => {
            const bullJob = sandbox.createStubInstance(Bull.Job);
            const job = {
                augurCode: 'GHIJK',
                jobType: 'learning',
                jobCode: 1,
            };
            bullJob.data = job;

            const executionQueue = sandbox.createStubInstance(ExecutionQueue);
            executionQueue.executeBash = sandbox.mock(ExecutionQueue.prototype)
                .expects('executeBash')
                .never();

            executionQueue.executeJob.restore();
            executionQueue.executeJob(bullJob)
                .then(() => {
                    executionQueue.executeBash.verify();
                    done('`executeJob` should throw an error');
                })
                .catch(err => {
                    if (err.message === `Unknown jobClass for execution: ${job.jobClass}`) return done();
                    done(err);
                });
        });
    });

    describe('onProcessExecute', () => {
        it('should call the `reject` callback if an error occured and change the job state to failed', () => {
            const job = {
                jobCode: 1,
                jobClass: 'binary-classification',
            };
            const bullJob = sandbox.createStubInstance(Bull.Job);
            bullJob.data = job;
            bullJob.update = sandbox.mock(Bull.Job.prototype)
                .expects('update')
                .once()
                .withExactArgs(jobStateMatcher(jobStates.FAILED))
                .returns(Promise.resolve());

            const error = new Error('An Error occured');

            const reject = sandbox.spy();

            const executionQueue = new ExecutionQueue('spark');
            executionQueue.onProcessExecute(error, job, bullJob, reject);

            chai.expect(reject.callCount).to.be.equal(1);
            bullJob.update.verify();
        });

        it('should not call the `reject` callback if no error occured', () => {
            const job = {
                jobCode: 1,
                jobClass: 'binary-classification',
            };
            const bullJob = sandbox.createStubInstance(Bull.Job);
            bullJob.data = job;
            bullJob.update = sandbox.mock(Bull.Job.prototype)
                .expects('update')
                .never();

            const reject = sandbox.spy();

            const executionQueue = new ExecutionQueue('spark');
            executionQueue.onProcessExecute(null, job, bullJob, reject);

            chai.expect(reject.callCount).to.be.equal(0);
            bullJob.update.verify();
        });
    });

    describe('onStatusChange', () => {
        it('should call the `resolve` callback if the Job executed, remove all listeners and change the job state to executed', () => {
            const job = {
                jobCode: 1,
                jobClass: 'binary-classification',
            };
            const bullJob = sandbox.createStubInstance(Bull.Job);
            bullJob.data = job;
            bullJob.update = sandbox.mock(Bull.Job.prototype)
                .expects('update')
                .once()
                .withExactArgs(jobStateMatcher(jobStates.EXECUTED))
                .returns(Promise.resolve());

            const removeAllListenersMock = sandbox.mock(EventEmitter.prototype);
            removeAllListenersMock.expects('removeAllListeners')
                .once()
                .withExactArgs(`${executionTypes[job.jobClass]}:${job.jobCode}`);

            const resolve = sandbox.spy();
            const reject = sandbox.spy();

            const executionQueue = new ExecutionQueue('spark');
            executionQueue.onStatusChange(jobStates.EXECUTED, job, bullJob, resolve, reject);

            bullJob.update.verify();
            removeAllListenersMock.verify();
            chai.expect(resolve.callCount).to.be.equal(1);
            chai.expect(reject.callCount).to.be.equal(0);
        });

        it('should call the `reject` callback if the Job failed, remove all listeners and change the job state to failed', () => {
            const job = {
                jobCode: 1,
                jobClass: 'binary-classification',
            };
            const bullJob = sandbox.createStubInstance(Bull.Job);
            bullJob.data = job;
            bullJob.update = sandbox.mock(Bull.Job.prototype)
                .expects('update')
                .once()
                .withExactArgs(jobStateMatcher(jobStates.FAILED))
                .returns(Promise.resolve());

            const removeAllListenersMock = sandbox.mock(EventEmitter.prototype);
            removeAllListenersMock.expects('removeAllListeners')
                .once()
                .withExactArgs(`${executionTypes[job.jobClass]}:${job.jobCode}`);

            const resolve = sandbox.spy();
            const reject = sandbox.spy();

            const executionQueue = new ExecutionQueue('spark');
            executionQueue.onStatusChange(jobStates.FAILED, job, bullJob, resolve, reject);

            bullJob.update.verify();
            removeAllListenersMock.verify();
            chai.expect(resolve.callCount).to.be.equal(0);
            chai.expect(reject.callCount).to.be.equal(1);
        });
    });
});