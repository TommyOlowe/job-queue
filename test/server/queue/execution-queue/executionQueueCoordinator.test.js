const chai = require('chai');
const sinon = require('sinon');

const executionTypes = require('../../../../src/server/queue/const/executionTypes');
const ExecutionQueue = require('../../../../src/server/queue/execution-queue/executionQueue');
const executionQueueCoordinator = require('../../../../src/server/queue/execution-queue/executionQueueCoordinator');

const executionQueueMatcher = (executionType) =>
    sinon.match(value => value.executionType === executionType);

describe('Execution Queue Coordinator', () => {
    /** @type {sinon.SinonSandbox} */
    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('addJob', () => {
        it('should create an Execution Queue with the Job\'s Execution Type and add the Job to it', done => {
            const job = {
                jobClass: 'binary-classification',
            };
            const jobExecutionType = executionTypes[job.jobClass];
            const addJobMock = sandbox.mock(ExecutionQueue.prototype);
            addJobMock.expects('addJob')
                .once()
                .withExactArgs(job)
                .returns(Promise.resolve(job));
            const executionQueueInstance = new ExecutionQueue(jobExecutionType);

            const mapHasMock = sandbox.mock(Map.prototype);
            mapHasMock.expects('has')
                .once()
                .withExactArgs(jobExecutionType)
                .returns(false);

            const mapSetMock = sandbox.mock(Map.prototype);
            mapSetMock.expects('set')
                .once()
                .withExactArgs(jobExecutionType, executionQueueMatcher(jobExecutionType));

            const mapGetMock = sandbox.mock(Map.prototype);
            mapGetMock.expects('get')
                .once()
                .withExactArgs(jobExecutionType)
                .returns(executionQueueInstance);

            executionQueueCoordinator.addJob(job)
                .then(resultJob => {
                    addJobMock.verify();
                    mapHasMock.verify();
                    mapSetMock.verify();
                    mapGetMock.verify();
                    chai.expect(resultJob).to.be.equal(job);
                    done();
                })
                .catch(err => done(err));
        });

        it('should add the Job to the corresponding Execution Queue', done => {
            const job = {
                jobClass: 'binary-classification',
            };
            const jobExecutionType = executionTypes[job.jobClass];
            const addJobMock = sandbox.mock(ExecutionQueue.prototype);
            addJobMock.expects('addJob')
                .once()
                .withExactArgs(job)
                .returns(Promise.resolve(job));
            const executionQueueInstance = new ExecutionQueue(jobExecutionType);

            const mapHasMock = sandbox.mock(Map.prototype);
            mapHasMock.expects('has')
                .once()
                .withExactArgs(jobExecutionType)
                .returns(true);

            const mapSetMock = sandbox.mock(Map.prototype);
            mapSetMock.expects('set')
                .never();

            const mapGetMock = sandbox.mock(Map.prototype);
            mapGetMock.expects('get')
                .once()
                .withExactArgs(jobExecutionType)
                .returns(executionQueueInstance);

            executionQueueCoordinator.addJob(job)
                .then(resultJob => {
                    addJobMock.verify();
                    mapHasMock.verify();
                    mapSetMock.verify();
                    mapGetMock.verify();
                    chai.expect(resultJob).to.be.equal(job);
                    done();
                })
                .catch(err => done(err));
        });
    });

    describe('getSummary', () => {
        it('should return a Promise that resolves with an empty array', (done) => {
            const executionQueuesStub = sandbox.stub(executionQueueCoordinator, 'executionQueues');
            executionQueuesStub.value(new Map());

            const summaryPromise = executionQueueCoordinator.getSummary();
            summaryPromise.then(summary => {
                chai.expect(summary).to.be.an('array').that.is.empty;
                done();
            }).catch(err => done(err));
        });
    });

    describe('getBullJob', () => {
        it('should return a Promise that resolves with the Bull Job', done => {
            const job = {
                bullJobID: 1,
                jobClass: 'binary-classification',
            };
            const bullJob = {
                id: job.bullJobID,
                data: job,
            };
            const getJobByIDMock = sandbox.mock(ExecutionQueue.prototype);
            getJobByIDMock.expects('getJobByID')
                .once()
                .withExactArgs(job.bullJobID)
                .returns(bullJob);
            const executionType = executionTypes[job.jobClass];
            const executionQueueInstance = new ExecutionQueue(executionType);

            const mapHasMock = sandbox.mock(Map.prototype);
            mapHasMock.expects('has')
                .once()
                .withExactArgs(executionType)
                .returns(true);

            const mapGetMock = sandbox.mock(Map.prototype);
            mapGetMock.expects('get')
                .once()
                .withExactArgs(executionType)
                .returns(executionQueueInstance);

            executionQueueCoordinator.getBullJob(job)
                .then(bullJob => {
                    getJobByIDMock.verify();
                    mapHasMock.verify();
                    mapGetMock.verify();
                    chai.expect(bullJob).to.deep.equal(bullJob);
                    done();
                }).catch(err => done(err));
        });

        it('should return a Promise that resolves with null if the Job does not exist', done => {
            const job = {
                bullJobID: 1,
                jobClass: 'binary-classification',
            };
            const getJobByIDMock = sandbox.mock(ExecutionQueue.prototype);
            getJobByIDMock.expects('getJobByID')
                .never();
            const executionType = executionTypes[job.jobClass];

            const mapHasMock = sandbox.mock(Map.prototype);
            mapHasMock.expects('has')
                .once()
                .withExactArgs(executionType)
                .returns(false);

            const mapGetMock = sandbox.mock(Map.prototype);
            mapGetMock.expects('get')
                .never();

            executionQueueCoordinator.getBullJob(job)
                .then(bullJob => {
                    getJobByIDMock.verify();
                    mapHasMock.verify();
                    mapGetMock.verify();
                    chai.expect(bullJob).to.be.null;
                    done();
                }).catch(err => done(err));
        });
    });

    describe('restoreExecutionQueues', () => {
        it('should restore all Execution Queues for all types', () => {
            const executionTypesKeys = Object.keys(executionTypes);

            const mapHasStub = sandbox.stub(Map.prototype, 'has');
            executionTypesKeys
                .forEach(jobClass => mapHasStub.withArgs(executionTypes[jobClass]).returns(false));
            mapHasStub.throws(new Error('Map#has() must be called with an Execution Type'));

            const mapSetStub = sandbox.stub(Map.prototype, 'set');
            executionTypesKeys
                .forEach(jobClass => mapSetStub.withArgs(executionTypes[jobClass], executionQueueMatcher(executionTypes[jobClass])).returns(true));
            mapSetStub.throws(new Error('Map#set() must be called with an Execution Type and Execution Queue Instance'));

            executionQueueCoordinator.restoreExecutionQueues();
        });

        it('should not restore already existing Execution Queues', () => {
            const executionTypesKeys = Object.keys(executionTypes);

            const mapHasStub = sandbox.stub(Map.prototype, 'has');
            executionTypesKeys
                .forEach(jobClass => mapHasStub.withArgs(executionTypes[jobClass]).returns(true));
            mapHasStub.throws(new Error('Map#has() must be called with an Execution Type'));

            const mapSetStub = sandbox.stub(Map.prototype, 'set');
            mapSetStub.throws(new Error('Map#set() should never be called'));

            executionQueueCoordinator.restoreExecutionQueues();
        });
    });
});
