const chai = require('chai');
const sinon = require('sinon');
const supertest = require('supertest');

const app = require('../../../src/server/server');
const manager = require('../../../src/server/queue/manager');
const activities = require('../../../src/server/activityLogger');
const jobStates = require('../../../src/server/queue/const/jobStates');
const augurQueueCoordinator = require('../../../src/server/queue/augur-queue/augurQueueCoordinator');
const executionQueueCoordinator = require('../../../src/server/queue/execution-queue/executionQueueCoordinator');

describe('Queue Router', () => {
    /** @type {sinon.SinonSandbox} */
    let sandbox;
    /** @type {supertest.SuperTest<supertest.Test>} */
    let request;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        request = supertest(app);
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('GET /api/queue', () => {
        it('should return a summary of all the queues and activities', done => {
            const augurQueues = { augurCode: 'ABCDE', queue: [] };
            const augurGetSummary = sandbox.mock(augurQueueCoordinator);
            augurGetSummary.expects('getSummary')
                .once()
                .withExactArgs()
                .returns(Promise.resolve(augurQueues));

            const executionQueues = { executionType: 'spark', queue: [] };
            const executionGetSummary = sandbox.mock(executionQueueCoordinator);
            executionGetSummary.expects('getSummary')
                .once()
                .withExactArgs()
                .returns(Promise.resolve(executionQueues));

            const activitiesSummary = [];
            const activitiesStub = sandbox.stub(activities, 'activities');
            activitiesStub.value(activitiesSummary);

            request.get('/api/queue/')
                .end((err, res) => {
                    if (err) return done(err);

                    augurGetSummary.verify();
                    executionGetSummary.verify();

                    chai.expect(res.body.augurQueues).to.be.deep.equal(augurQueues);
                    chai.expect(res.body.executionQueues).to.be.deep.equal(executionQueues);
                    chai.expect(res.body.activities).to.be.deep.equal(activitiesSummary);

                    done();
                });
        });
    });

    describe('POST /api/queue/jobGroup', () => {
        it('should add a Job Group', done => {
            const jobGroup = {
                jobGroupCode: 1,
                augurCode: 'ABCDE',
                jobGroupType: 'initial-learning',
                jobs: [{
                    jobCode: 1,
                }],
            };
            const addJobGroupMock = sandbox.mock(manager);
            addJobGroupMock.expects('addJobGroup')
                .once()
                .withExactArgs(jobGroup)
                .returns(Promise.resolve());

            request.post('/api/queue/jobGroup')
                .send({ jobGroup: jobGroup })
                .expect(201)
                .end((err, res) => {
                    if (err) return done(err);

                    addJobGroupMock.verify();

                    chai.expect(res.body.status).to.be.equal('success');
                    chai.expect(res.body.jobGroupCode).to.be.equal(jobGroup.jobGroupCode);
                    chai.expect(res.body.jobCodes).to.be.deep.equal(jobGroup.jobs.map(job => job.jobCode));

                    done();
                });
        });

        it('should not add the Job Group if it is missing augurCode and fail with code 501', done => {
            const jobGroup = {
                jobGroupCode: 1,
                jobGroupType: 'initial-learning',
                jobs: [{
                    jobCode: 1,
                }],
            };
            const addJobGroupMock = sandbox.mock(manager);
            addJobGroupMock.expects('addJobGroup')
                .never();

            request.post('/api/queue/jobGroup')
                .send({ jobGroup: jobGroup })
                .expect(501)
                .end((err, res) => {
                    if (err) return done(err);

                    addJobGroupMock.verify();

                    chai.expect(res.body.status).to.be.equal('augurCode is undefined');

                    done();
                });
        });
    });

    describe('PUT /api/queue/job/state', () => {
        it('should update the jobs state successfully', done => {
            const jobCode = '1';
            const newJobState = jobStates.EXECUTING;

            const updateJobStateMock = sandbox.mock(manager);
            updateJobStateMock.expects('updateJobState')
                .once()
                .withExactArgs(jobCode, newJobState)
                .returns(manager.updateStateResponse.OK);

            request.put('/api/queue/job/state')
            .send({jobCode: jobCode, newJobState: newJobState})
            .expect(201)
            .end((err, res) => {
                if (err) return done(err);

                updateJobStateMock.verify();

                chai.expect(res.body.status).to.be.equal('success');

                done();
            });
        });

        it('should fail updating the job\'s state with code 501', done => {
            const jobCode = '1';
            const newJobState = jobStates.EXECUTING;

            const updateJobStateMock = sandbox.mock(manager);
            updateJobStateMock.expects('updateJobState')
                .once()
                .withExactArgs(jobCode, newJobState)
                .returns(false);

            request.put('/api/queue/job/state')
            .send({jobCode: jobCode, newJobState: newJobState})
            .expect(501)
            .end((err, res) => {
                if (err) return done(err);

                updateJobStateMock.verify();

                chai.expect(res.body.status).to.be.equal('error');

                done();
            });
        });
    });
});
