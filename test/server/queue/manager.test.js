const chai = require('chai');
const sinon = require('sinon');

const manager = require('../../../src/server/queue/manager');
const { redisClient } = require('../../../src/server/redis/db');
const { emitter } = require('../../../src/server/queue/jobEventBus');
const jobStates = require('../../../src/server/queue/const/jobStates');
const jobGroupStates = require('../../../src/server/queue/const/jobGroupStates');
const executionTypes = require('../../../src/server/queue/const/executionTypes');
const jobGroupTypePriorities = require('../../../src/server/queue/const/jobGroupTypePriorities');
const augurQueueCoordinator = require('../../../src/server/queue/augur-queue/augurQueueCoordinator');
const executionQueueCoordinator = require('../../../src/server/queue/execution-queue/executionQueueCoordinator');

describe('Queue Manager', () => {
    /** @type {sinon.SinonSandbox} */
    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('addJobGroup', () => {
        it('should add a Job Group to the Augur Queue', done => {
            const jobGroup = {
                jobGroupCode: 1,
                jobGroupState: jobGroupStates.WAITING,
                jobGroupTypePriority: jobGroupTypePriorities.learning,
                jobs: [{
                    jobCode: 1,
                    jobGroupCode: 1,
                    jobClass: 'binary-classification',
                    jobState: jobStates.WAITING,
                    augurCode: 'ABCDE'
                }],
            };
            const hmsetArgs = [];
            jobGroup.jobs.forEach(job => hmsetArgs.push(job.jobCode.toString(), job.jobClass));

            const hmsetMock = sandbox.mock(redisClient);
            hmsetMock.expects('hmset')
                .once()
                .withExactArgs('alta-sigma:manager:jobs', ...hmsetArgs)
                .returns(Promise.resolve());

            const mapSetMock = sandbox.mock(Map.prototype);
            mapSetMock.expects('set')
                .once()
                .withExactArgs(hmsetArgs[0], hmsetArgs[1]);

            const addJobGroupMock = sandbox.mock(augurQueueCoordinator);
            addJobGroupMock.expects('addJobGroup')
                .once()
                .withExactArgs(jobGroup)
                .returns(Promise.resolve());

            manager.addJobGroup(jobGroup)
                .then(() => {
                    hmsetMock.verify();
                    mapSetMock.verify();
                    addJobGroupMock.verify();

                    done();
                })
                .catch(err => done(err));
        });
    });

    describe('updateJobState', () => {
        it('should emit a job state event and return true when job-class is given', () => {
            const jobCode = '1';
            const newState = jobStates.EXECUTING;
            const jobClass = 'binary-classification';

            const mapGetMock = sandbox.mock(Map.prototype);
            mapGetMock.expects('get')
                .once()
                .withExactArgs(jobCode)
                .returns(jobClass);

            const emitterMock = sandbox.mock(emitter);
            emitterMock.expects('emit')
                .once()
                .withExactArgs(`${executionTypes[jobClass]}:${jobCode}`, newState);

            const result = manager.updateJobState(jobCode, newState);

            mapGetMock.verify();
            emitterMock.verify();
            chai.expect(result).to.be.equal(manager.updateStateResponse.OK)
        });

        it('should not emit a job state event and return false when job-class misses', () => {
            const jobCode = '1';
            const newState = jobStates.EXECUTING;

            const mapGetMock = sandbox.mock(Map.prototype);
            mapGetMock.expects('get')
                .once()
                .withExactArgs(jobCode)
                .returns(null);

            const emitterMock = sandbox.mock(emitter);
            emitterMock.expects('emit')
                .never();

            const result = manager.updateJobState(jobCode, newState);

            mapGetMock.verify();
            emitterMock.verify();
            chai.expect(result).to.be.equal(manager.updateStateResponse.NOT_FOUND)
        });
    });

    describe('restoreState', () => {
        it('should restore jobs and Augur and Execution Queues', done => {
            const jobCodes = ['1'];
            const jobClasses = ['binary-classification'];

            const hkeysMock = sandbox.mock(redisClient);
            hkeysMock.expects('hkeys')
                .once()
                .withExactArgs('alta-sigma:manager:jobs')
                .returns(Promise.resolve(jobCodes));

            const hmgetMock = sandbox.mock(redisClient);
            hmgetMock.expects('hmget')
                .once()
                .withExactArgs('alta-sigma:manager:jobs', jobCodes)
                .returns(Promise.resolve(jobClasses));

            const restoreExecutionQueuesMock = sandbox.mock(executionQueueCoordinator);
            restoreExecutionQueuesMock.expects('restoreExecutionQueues')
                .once()
                .withExactArgs()
                .returns(Promise.resolve());

            const restoreAugurQueuesMock = sandbox.mock(augurQueueCoordinator);
            restoreAugurQueuesMock.expects('restoreAugurQueues')
                .once()
                .withExactArgs()
                .returns(Promise.resolve());

            manager.restoreState()
                .then(() => {
                    hkeysMock.verify();
                    hmgetMock.verify();
                    restoreExecutionQueuesMock.verify();
                    restoreAugurQueuesMock.verify();

                    done();
                })
                .catch(err => done(err));
        });
    });
});
