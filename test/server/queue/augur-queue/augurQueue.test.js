const chai = require('chai');
const Bull = require('bull');
const sinon = require('sinon');

const jobStates = require('../../../../src/server/queue/const/jobStates');
const AugurQueue = require('../../../../src/server/queue/augur-queue/augurQueue');
const jobGroupStates = require('../../../../src/server/queue/const/jobGroupStates');
const executionQueueCoordinator = require('../../../../src/server/queue/execution-queue/executionQueueCoordinator');

const jobGroupFailMatcher = () => sinon.match(jobGroup => jobGroup.jobGroupState === jobGroupStates.FAILED
    && jobGroup.jobs.reduce((previousVal, job) => previousVal && job.jobState === jobStates.FAILED, true));

describe('Augur Queue', () => {
    /** @type {sinon.SinonSandbox} */
    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('addJobGroup', () => {
        it('should add and return a Job after remove similar Jobs', done => {
            const jobGroup = {
                jobGroupCode: 1,
                jobGroupType: 'learning',
            };
            const similarWaitingJobGroup = sandbox.createStubInstance(Bull.Job);
            similarWaitingJobGroup.data = {
                jobGroupCode: 2,
                jobGroupType: 'learning',
            };
            similarWaitingJobGroup.remove.returns(Promise.resolve());

            const waitingJobGroup = sandbox.createStubInstance(Bull.Job);
            waitingJobGroup.data = {
                jobGroupCode: 2,
                jobGroupType: 'evaluation',
            };

            const getWaitingMock = sandbox.mock(Bull.prototype);
            getWaitingMock.expects('getWaiting')
                .once()
                .withExactArgs()
                .returns(Promise.resolve([similarWaitingJobGroup, waitingJobGroup]));

            const addMock = sandbox.mock(Bull.prototype);
            addMock.expects('add')
                .once()
                .withExactArgs(jobGroup, { priority: jobGroup.jobGroupTypePriority })
                .returns(Promise.resolve(jobGroup));

            const augurQueue = new AugurQueue('def', 'GHIJK');
            augurQueue.addJobGroup(jobGroup)
                .then(job => {
                    getWaitingMock.verify();
                    addMock.verify();
                    chai.expect(similarWaitingJobGroup.remove.callCount).to.be.equal(1);
                    chai.expect(waitingJobGroup.remove.callCount).to.be.equal(0);
                    done();
                })
                .catch(err => done(err));
        });
    });

    describe('pushJob', () => {
        it('should push Job to the Execution Queue and wait for it to complete', done => {
            const jobGroup = {
                jobGroupState: jobGroupStates.WAITING,
                jobs: [{
                    jobState: jobStates.WAITING,
                }],
            };
            const bullJobGroup = sandbox.createStubInstance(Bull.Job);
            bullJobGroup.data = jobGroup;

            const bullJob = sandbox.createStubInstance(Bull.Job);
            bullJob.id = 1;
            bullJob.data = jobGroup.jobs[0];

            bullJobGroup.update = sandbox.mock(Bull.Job.prototype)
                .expects('update')
                .exactly(4)
                .returns(Promise.resolve())

            bullJob.finished = sandbox.mock(Bull.Job.prototype)
                .expects('finished')
                .once()
                .returns(Promise.resolve())

            const addJobMock = sandbox.mock(executionQueueCoordinator);
            addJobMock.expects('addJob')
                .once()
                .withExactArgs(jobGroup.jobs[0])
                .returns(Promise.resolve(bullJob));

            const augurQueue = new AugurQueue('def', 'GHIJK');
            augurQueue.pushJob(bullJobGroup)
                .then(() => {
                    addJobMock.verify();
                    bullJobGroup.update.verify();
                    bullJob.finished.verify();
                    done();
                })
                .catch(err => done(err));
        });

        it('should retrieve the Job from the Execution Queue and wait for it to complete', done => {
            const jobGroup = {
                jobGroupState: jobGroupStates.WAITING,
                jobs: [{
                    jobState: jobStates.EXECUTING,
                }],
            };
            const bullJobGroup = sandbox.createStubInstance(Bull.Job);
            bullJobGroup.data = jobGroup;

            const bullJob = sandbox.createStubInstance(Bull.Job);
            bullJob.id = 1;
            bullJob.data = jobGroup.jobs[0];

            bullJobGroup.update = sandbox.mock(Bull.Job.prototype)
                .expects('update')
                .exactly(3)
                .returns(Promise.resolve())

            bullJob.finished = sandbox.mock(Bull.Job.prototype)
                .expects('finished')
                .once()
                .returns(Promise.resolve())

            const addJobMock = sandbox.mock(executionQueueCoordinator);
            addJobMock.expects('getBullJob')
                .once()
                .withExactArgs(jobGroup.jobs[0])
                .returns(Promise.resolve(bullJob));

            const augurQueue = new AugurQueue('def', 'GHIJK');
            augurQueue.pushJob(bullJobGroup)
                .then(() => {
                    addJobMock.verify();
                    bullJobGroup.update.verify();
                    bullJob.finished.verify();
                    done();
                })
                .catch(err => done(err));
        });

        it('should fail Augur Queue and throw an error', done => {
            const jobGroup = {
                jobGroupState: jobGroupStates.WAITING,
                jobs: [{
                    jobState: jobStates.FAILED,
                }],
            };
            const bullJobGroup = sandbox.createStubInstance(Bull.Job);
            bullJobGroup.data = jobGroup;

            bullJobGroup.update = sandbox.mock(Bull.Job.prototype)
                .expects('update')
                .exactly(1)
                .returns(Promise.resolve())

            const failJobGroupMock = sandbox.mock(AugurQueue.prototype);
            failJobGroupMock.expects('failJobGroup')
                .once()
                .withExactArgs(bullJobGroup, 0)
                .returns(Promise.resolve());

            const augurQueue = new AugurQueue('def', 'GHIJK');
            augurQueue.pushJob(bullJobGroup)
                .then(() => {
                    done('`pushJob` should throw an error')
                })
                .catch(err => {
                    if (err.message === 'Job Group Failed!') {
                        failJobGroupMock.verify();
                        bullJobGroup.update.verify();
                        return done();
                    }
                    done(err);
                });
        });
    });

    describe('getSummary', () => {
        it('should return active, waiting and failed Job Groups', done => {
            const jobGroups = [{
                id: 1,
                data: {
                    jobGroupCode: 1,
                    jobGroupType: 'learning',
                    jobs: {
                        jobCode: 2,
                        jobClass: 'binary-classification',
                    },
                }
            }];

            const getJobsMock = sandbox.mock(Bull.prototype);
            getJobsMock.expects('getJobs')
                .once()
                .withExactArgs(['active', 'waiting', 'failed'])
                .returns(Promise.resolve(jobGroups));

            const augurQueue = new AugurQueue('def', 'GHIJK');

            augurQueue.getSummary()
                .then(jobGroupsSummary => {
                    getJobsMock.verify();
                    chai.expect(jobGroupsSummary).to.deep.equal(jobGroups.map(jobGroup => jobGroup.data));
                    done();
                })
                .catch(err => done(err));
        });
    });

    describe('failJobGroup', () => {
        it('should fail Jobs in the Job Group and fail the Augur Queue', done => {
            const activeJobGroup = {
                jobGroupCode: 1,
                jobGroupType: 'learning',
                augurCode: 'GHIJK',
                jobGroupState: jobGroupStates.EXECUTING,
                jobs: [{
                    jobCode: 1,
                    jobClass: 'binary-classification',
                    jobState: jobStates.EXECUTING,
                }, {
                    jobCode: 2,
                    jobClass: 'dev-wait',
                    jobState: jobStates.WAITING,
                }],
            };
            const waitingJobGroup = {
                jobGroupCode: 3,
                jobGroupType: 'learning',
                augurCode: 'GHIJK',
                jobGroupState: jobGroupStates.WAITING,
                jobs: [{
                    jobCode: 1,
                    jobClass: 'binary-classification',
                    jobState: jobStates.WAITING,
                }, {
                    jobCode: 2,
                    jobClass: 'dev-wait',
                    jobState: jobStates.WAITING,
                }],
            };
            const activeBullJob = sandbox.createStubInstance(Bull.Job);
            activeBullJob.data = activeJobGroup;
            activeBullJob.update.withArgs(jobGroupFailMatcher()).returns(Promise.resolve())
            activeBullJob.update.returns(Promise.reject('Must be called with a Failed Job Group'));

            const waitingBullJob = sandbox.createStubInstance(Bull.Job);
            waitingBullJob.data = waitingJobGroup;
            waitingBullJob.moveToFailed.withArgs().returns(Promise.resolve());

            const waitingJobGroups = [waitingBullJob];

            const getJobsMock = sandbox.mock(Bull.prototype);
            getJobsMock.expects('getJobs')
                .once()
                .withExactArgs(['waiting'])
                .returns(Promise.resolve(waitingJobGroups));

            const augurQueue = new AugurQueue('def', 'GHIJK');
            augurQueue.failJobGroup(activeBullJob, 0)
                .then(() => {
                    getJobsMock.verify();
                    chai.expect(activeBullJob.update.callCount).to.be.equal(1);
                    chai.expect(waitingBullJob.moveToFailed.callCount).to.be.equal(1);
                    done();
                })
                .catch(err => done(err));
        });
    });
});