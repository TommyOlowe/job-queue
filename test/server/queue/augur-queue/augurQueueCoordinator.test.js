const chai = require('chai');
const sinon = require('sinon');

const { redisClient } = require('../../../../src/server/redis/db');
const AugurQueue = require('../../../../src/server/queue/augur-queue/augurQueue');
const augurQueueCoordinator = require('../../../../src/server/queue/augur-queue/augurQueueCoordinator');

const augurQueueMatcher = (augurCode, habitatCode) =>
    sinon.match(value => value.augurCode === augurCode
        && value.habitatCode === habitatCode);

describe('Augur Queue Coordinator', () => {

    /**@type {sinon.SinonSandbox} */
    let sandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('addJobGroup', () => {
        it('should create an Augur Queue and add the Job Group after adding the augur and habitat codes to redis', (done) => {
            const jobGroup = {
                augurCode: 'ABCDE',
                habitatCode: 'def',
            };
            const addJobGroupMock = sandbox.mock(AugurQueue.prototype);
            addJobGroupMock.expects('addJobGroup')
                .once()
                .withExactArgs(jobGroup)
                .returns(Promise.resolve());

            const augurQueueInstance = new AugurQueue(jobGroup.habitatCode, jobGroup.augurCode);

            const hsetMock = sandbox.mock(redisClient);
            hsetMock.expects('hset')
                .once()
                .withExactArgs('alta-sigma:aqc:aq', jobGroup.augurCode, jobGroup.habitatCode)
                .returns(Promise.resolve());

            const mapHasMock = sandbox.mock(Map.prototype);
            mapHasMock.expects('has')
                .once()
                .withExactArgs(jobGroup.augurCode)
                .returns(false);

            const mapSetMock = sandbox.mock(Map.prototype);
            mapSetMock.expects('set')
                .once()
                .withExactArgs(jobGroup.augurCode, augurQueueMatcher(jobGroup.augurCode, jobGroup.habitatCode));

            const mapGetMock = sandbox.mock(Map.prototype);
            mapGetMock.expects('get')
                .once()
                .withExactArgs(jobGroup.augurCode)
                .returns(augurQueueInstance);

            augurQueueCoordinator.addJobGroup(jobGroup)
                .then(() => {
                    hsetMock.verify();
                    mapHasMock.verify();
                    mapSetMock.verify();
                    mapGetMock.verify();
                    addJobGroupMock.verify();
                    done();
                }).catch(err => done(err));
        });

        it('should add the Job Group to the corresponding Augur Queue after adding the augur and habitat codes to redis', (done) => {
            const jobGroup = {
                augurCode: 'ABCDE',
                habitatCode: 'def',
            };
            const addJobGroupMock = sandbox.mock(AugurQueue.prototype);
            addJobGroupMock.expects('addJobGroup')
                .once()
                .withExactArgs(jobGroup)
                .returns(Promise.resolve());

            const augurQueueInstance = new AugurQueue(jobGroup.habitatCode, jobGroup.augurCode);

            const hsetMock = sandbox.mock(redisClient);
            hsetMock.expects('hset')
                .once()
                .withExactArgs('alta-sigma:aqc:aq', jobGroup.augurCode, jobGroup.habitatCode)
                .returns(Promise.resolve());

            const mapHasMock = sandbox.mock(Map.prototype);
            mapHasMock.expects('has')
                .once()
                .withExactArgs(jobGroup.augurCode)
                .returns(true);

            const mapSetMock = sandbox.mock(Map.prototype);
            mapSetMock.expects('set')
                .never();

            const mapGetMock = sandbox.mock(Map.prototype);
            mapGetMock.expects('get')
                .once()
                .withExactArgs(jobGroup.augurCode)
                .returns(augurQueueInstance);

            augurQueueCoordinator.addJobGroup(jobGroup)
                .then(() => {
                    hsetMock.verify();
                    mapHasMock.verify();
                    mapSetMock.verify();
                    mapGetMock.verify();
                    addJobGroupMock.verify();
                    done();
                }).catch(err => done(err));
        });

        it('should do nothing if adding the augur and habitat codes to redis fail', (done) => {
            const jobGroup = {
                augurCode: 'ABCDE',
                habitatCode: 'def',
            };
            const addJobGroupMock = sandbox.mock(AugurQueue.prototype);
            addJobGroupMock.expects('addJobGroup')
                .never();

            const hsetMock = sandbox.mock(redisClient);
            hsetMock.expects('hset')
                .once()
                .withExactArgs('alta-sigma:aqc:aq', jobGroup.augurCode, jobGroup.habitatCode)
                .returns(Promise.reject(new Error('Redis Error')));

            const mapHasMock = sandbox.mock(Map.prototype);
            mapHasMock.expects('has')
                .never();

            const mapSetMock = sandbox.mock(Map.prototype);
            mapSetMock.expects('set')
                .never();

            const mapGetMock = sandbox.mock(Map.prototype);
            mapGetMock.expects('get')
                .never();

            augurQueueCoordinator.addJobGroup(jobGroup)
                .then(() => done('Promise should reject resulting in catch block running !'))
                .catch(err => {
                    if (err.message !== 'Redis Error') return done(err);
                    hsetMock.verify();
                    mapHasMock.verify();
                    mapSetMock.verify();
                    mapGetMock.verify();
                    addJobGroupMock.verify();
                    done();
                });
        });
    });

    describe('getSummary', () => {
        it('should return a Promise that resolves with an empty array', (done) => {
            const augurQueuesStub = sandbox.stub(augurQueueCoordinator, 'augurQueues');
            augurQueuesStub.value(new Map());

            const summaryPromise = augurQueueCoordinator.getSummary();
            summaryPromise.then(summary => {
                chai.expect(summary).to.be.an('array').that.is.empty;
                done();
            }).catch(err => done(err));
        });
    });

    describe('restoreAugurQueues', () => {
        it('should restore Augur Queues using augur and habitat codes stored in redis', (done) => {
            const augurCodes = ['ABCDE'];
            const habitatCodes = ['def'];

            const hkeysMock = sandbox.mock(redisClient);
            hkeysMock.expects('hkeys')
                .once()
                .withExactArgs('alta-sigma:aqc:aq')
                .returns(Promise.resolve(augurCodes));

            const hmgetMock = sandbox.mock(redisClient);
            hmgetMock.expects('hmget')
                .once()
                .withExactArgs('alta-sigma:aqc:aq', augurCodes)
                .returns(Promise.resolve(habitatCodes));

            const mapHasMock = sandbox.mock(Map.prototype);
            mapHasMock.expects('has')
                .once()
                .withExactArgs(augurCodes[0])
                .returns(false);

            const mapSetMock = sandbox.mock(Map.prototype);
            mapSetMock.expects('set')
                .once()
                .withExactArgs(augurCodes[0], augurQueueMatcher(augurCodes[0], habitatCodes[0]));

            augurQueueCoordinator.restoreAugurQueues()
                .then(() => {
                    hkeysMock.verify();
                    hmgetMock.verify();
                    mapHasMock.verify();
                    mapSetMock.verify();
                    done();
                }).catch(err => done(err));
        });

        it('should do nothing if no augur codes are stored', (done) => {
            const hkeysMock = sandbox.mock(redisClient);
            hkeysMock.expects('hkeys')
                .once()
                .withExactArgs('alta-sigma:aqc:aq')
                .returns(Promise.resolve([]));

            const hmgetMock = sandbox.mock(redisClient);
            hmgetMock.expects('hmget')
                .never();

            const mapHasMock = sandbox.mock(Map.prototype);
            mapHasMock.expects('has')
                .never();

            const mapSetMock = sandbox.mock(Map.prototype);
            mapSetMock.expects('set')
                .never();

            augurQueueCoordinator.restoreAugurQueues()
                .then(() => {
                    hkeysMock.verify();
                    hmgetMock.verify();
                    mapHasMock.verify();
                    mapSetMock.verify();
                    done();
                }).catch(err => done(err));
        });

        it('should restore an Augur Queue that is already running', done => {
            const augurCodes = ['ABCDE'];
            const habitatCodes = ['def'];

            const hkeysMock = sandbox.mock(redisClient);
            hkeysMock.expects('hkeys')
                .once()
                .withExactArgs('alta-sigma:aqc:aq')
                .returns(Promise.resolve(augurCodes));

            const hmgetMock = sandbox.mock(redisClient);
            hmgetMock.expects('hmget')
                .once()
                .withExactArgs('alta-sigma:aqc:aq', augurCodes)
                .returns(Promise.resolve(habitatCodes));

            const mapHasMock = sandbox.mock(Map.prototype);
            mapHasMock.expects('has')
                .once()
                .withExactArgs(augurCodes[0])
                .returns(true);

            const mapSetMock = sandbox.mock(Map.prototype);
            mapSetMock.expects('set')
                .never();

            augurQueueCoordinator.restoreAugurQueues()
                .then(() => {
                    hkeysMock.verify();
                    hmgetMock.verify();
                    mapHasMock.verify();
                    mapSetMock.verify();
                    done();
                }).catch(err => done(err));
        });
    });
});