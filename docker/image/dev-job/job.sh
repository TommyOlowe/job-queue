#!/usr/bin/env bash

# Report Job Start
curl --header "Content-Type: application/json" \
--request PUT \
--data "{\"jobCode\": \"${JOB_CODE}\",\"newJobState\": \"executing\"}" \
http://${QUEUE_API_HOST}:${QUEUE_API_PORT}/api/queue/job/state

# Simulate Job Execution
sleep $SECONDS

# Report Job Success
curl --header "Content-Type: application/json" \
--request PUT \
--data "{\"jobCode\": \"${JOB_CODE}\",\"newJobState\": \"executed\"}" \
http://${QUEUE_API_HOST}:${QUEUE_API_PORT}/api/queue/job/state
