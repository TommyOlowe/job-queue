#!/usr/bin/env bash
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

docker tag as/job-queue:${CI_COMMIT_SHA} $CI_REGISTRY_IMAGE/job-queue:$CI_COMMIT_SHA
docker push $CI_REGISTRY_IMAGE/job-queue:$CI_COMMIT_SHA

docker tag as/job-queue-cron-sidecar:${CI_COMMIT_SHA} $CI_REGISTRY_IMAGE/job-queue-cron-sidecar:$CI_COMMIT_SHA
docker push $CI_REGISTRY_IMAGE/job-queue-cron-sidecar:$CI_COMMIT_SHA
