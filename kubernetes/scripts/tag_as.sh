#!/usr/bin/env bash
TAG=$1

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

docker pull $CI_REGISTRY_IMAGE/job-queue:${CI_COMMIT_SHA}
docker tag $CI_REGISTRY_IMAGE/job-queue:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE/job-queue:$TAG
docker push $CI_REGISTRY_IMAGE/job-queue:$TAG

docker pull $CI_REGISTRY_IMAGE/job-queue-cron-sidecar:${CI_COMMIT_SHA}
docker tag $CI_REGISTRY_IMAGE/job-queue-cron-sidecar:${CI_COMMIT_SHA} $CI_REGISTRY_IMAGE/job-queue-cron-sidecar:$TAG
docker push $CI_REGISTRY_IMAGE/job-queue-cron-sidecar:$TAG
