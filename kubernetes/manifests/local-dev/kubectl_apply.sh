#!/usr/bin/env bash

kubectl apply -f cm_env_queue.yaml
kubectl apply -f deploy_queue.yaml
kubectl apply -f cr.yaml

# Create Clusterrolebinding
kubectl create clusterrolebinding queue --clusterrole=deployment-creator --serviceaccount=default:default

# Add ImagePullSecret to ServiceAccount
kubectl patch serviceaccount default -p '{"imagePullSecrets": [{"name": "regcred"}]}'
